package se.bengtsson.desc.graphics;

import se.bengtsson.desc.graphics.sprites.Sprite;

/**
 * Render class, render graphics to an array that represents the screen
 * 
 * @author Marcus Bengtsson
 * 
 */
public class Render {

	private int width, height;
	private int[] renderedPixels;

	public int xOffset, yOffset;

	/**
	 * Constructor, sets up the size of the array (screen) to render to
	 * 
	 * @param width
	 * @param height
	 */
	public Render(int width, int height) {
		this.width = width;
		this.height = height;
		renderedPixels = new int[width * height];

	}

	/**
	 * Returns the rendered pixels
	 * 
	 * @return the rendered pixels
	 */
	public int[] getRenderedPixels() {
		return renderedPixels;
	}

	/**
	 * Returns the with of the render area
	 * 
	 * @return the with of the render area
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * Returns the height of the render area
	 * 
	 * @return the height of the render area
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * Clears the render area
	 */
	public void clear() {
		for (int i = 0; i < renderedPixels.length; i++) {
			renderedPixels[i] = 0;
		}
	}

	/**
	 * Render the sprite of a tile on the correct position of the screen.
	 * 
	 * @param xp
	 *            x-position of tile to render
	 * @param yp
	 *            y-position of the tile to render
	 * @param sprite
	 *            sprite to render on the tile
	 */
	public void renderTile(int xp, int yp, Sprite sprite) {

		for (int y = 0; y < sprite.SIZE; y++) {
			// ya is the y-coordinate for the current pixel
			int ya = y + yp;
			for (int x = 0; x < sprite.SIZE; x++) {
				// xa is the x-coordinate for the current pixel
				int xa = x + xp;

				// breaks if the pixel is outside the screen, renders one tile
				// "extra" to the left and top to avoid
				// "tiling effect" on those sides, right and bottom are handeled
				// by the gamearea class
				if (xa < -sprite.SIZE || xa >= width || ya < -sprite.SIZE || ya >= height) {
					break;
				}
				// if the pixel is out of bounds render it at respective
				// 0-coordinate
				if (xa < 0) {
					xa = 0;
				}
				if (ya < 0) {
					ya = 0;
				}

				// copies the pixels from the sprite to the screen
				renderedPixels[xa + ya * width] = sprite.getSpritePixels()[x + y * sprite.SIZE];

			}
		}
	}

}
