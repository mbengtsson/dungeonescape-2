package se.bengtsson.desc.graphics.sprites;

/**
 * Sprite class, makes sprites from a sprite-sheet
 * 
 * @author Marcus Bengtsson
 * 
 */
public class Sprite {

	public final int SIZE;
	private int x, y;
	private int[] spritePixels;
	private SpriteSheet sheet;

	private static Sprite hwall1 = new Sprite(32, 0, 0, SpriteSheet.getTileSheet());
	private static Sprite hwall2 = new Sprite(32, 1, 0, SpriteSheet.getTileSheet());
	private static Sprite hwall3 = new Sprite(32, 2, 0, SpriteSheet.getTileSheet());
	private static Sprite lvwall = new Sprite(32, 1, 1, SpriteSheet.getTileSheet());
	private static Sprite rvwall = new Sprite(32, 2, 1, SpriteSheet.getTileSheet());
	private static Sprite blcorner = new Sprite(32, 0, 2, SpriteSheet.getTileSheet());
	private static Sprite brcorner = new Sprite(32, 1, 2, SpriteSheet.getTileSheet());
	private static Sprite nvwall = new Sprite(32, 3, 1, SpriteSheet.getTileSheet());
	private static Sprite tnvwall = new Sprite(32, 0, 1, SpriteSheet.getTileSheet());
	private static Sprite tlcorner = new Sprite(32, 3, 0, SpriteSheet.getTileSheet());
	private static Sprite trcorner = new Sprite(32, 4, 0, SpriteSheet.getTileSheet());
	private static Sprite lncorner = new Sprite(32, 2, 2, SpriteSheet.getTileSheet());
	private static Sprite rncorner = new Sprite(32, 3, 2, SpriteSheet.getTileSheet());
	private static Sprite floor = new Sprite(32, 3, 3, SpriteSheet.getTileSheet());
	private static Sprite stair = new Sprite(32, 4, 1, SpriteSheet.getTileSheet());

	private static Sprite boss = new Sprite(32, 4, 7, SpriteSheet.getTileSheet());
	private static Sprite mob = new Sprite(32, 5, 7, SpriteSheet.getTileSheet());
	private static Sprite player = new Sprite(32, 6, 7, SpriteSheet.getTileSheet());

	private static Sprite empty = new Sprite(32, 7, 7, SpriteSheet.getTileSheet());

	/**
	 * Constructor, creates a sprite
	 * 
	 * @param size
	 *            of the sprite
	 * @param x
	 *            x-coordinate on the sprite-sheet
	 * @param y
	 *            y-coordinate on the sprite-sheet
	 * @param sheet
	 *            the sprite-sheet to load from
	 */
	private Sprite(int size, int x, int y, SpriteSheet sheet) {
		SIZE = size;
		this.x = x * SIZE;
		this.y = y * SIZE;
		this.sheet = sheet;
		spritePixels = new int[SIZE * SIZE];
		load();
	}

	/**
	 * returns an array with the pixels of the sprite
	 * 
	 * @return an array with the pixels of the sprite
	 */
	public int[] getSpritePixels() {
		return spritePixels;
	}

	/**
	 * returns horizontal wall1 sprite
	 * 
	 * @return horizontal wall1 sprite
	 */
	public static Sprite getHwall1() {
		return hwall1;
	}

	/**
	 * returns horizontal wall2 sprite
	 * 
	 * @return horizontal wall2 sprite
	 */
	public static Sprite getHwall2() {
		return hwall2;
	}

	/**
	 * returns horizontal wall3 sprite
	 * 
	 * @return horizontal wall3 sprite
	 */
	public static Sprite getHwall3() {
		return hwall3;
	}

	/**
	 * returns left vertical wall sprite
	 * 
	 * @return left vertical wall sprite
	 */
	public static Sprite getLvwall() {
		return lvwall;
	}

	/**
	 * returns right vertical wall sprite
	 * 
	 * @return right vertical wall sprite
	 */
	public static Sprite getRvwall() {
		return rvwall;
	}

	/**
	 * returns bottom left corner sprite
	 * 
	 * @return bottom left corner sprite
	 */
	public static Sprite getBlcorner() {
		return blcorner;
	}

	/**
	 * returns bottom right corner sprite
	 * 
	 * @return bottom right corner sprite
	 */
	public static Sprite getBrcorner() {
		return brcorner;
	}

	/**
	 * returns narrow vertical wall sprite
	 * 
	 * @return narrow vertical wall sprite
	 */
	public static Sprite getNvwall() {
		return nvwall;
	}

	/**
	 * returns top narrow vertical wall sprite
	 * 
	 * @return top narrow vertical wall sprite
	 */
	public static Sprite getTnvwall() {
		return tnvwall;
	}

	/**
	 * returns top left corner sprite
	 * 
	 * @return top left corner sprite
	 */
	public static Sprite getTlcorner() {
		return tlcorner;
	}

	/**
	 * returns top right corner sprite
	 * 
	 * @return top right corner sprite
	 */
	public static Sprite getTrcorner() {
		return trcorner;
	}

	/**
	 * returns left narrow corner sprite
	 * 
	 * @return left narrow corner sprite
	 */
	public static Sprite getLncorner() {
		return lncorner;
	}

	/**
	 * returns right narrow corner sprite
	 * 
	 * @return right narrow corner sprite
	 */
	public static Sprite getRncorner() {
		return rncorner;
	}

	/**
	 * returns floor sprite
	 * 
	 * @return floor sprite
	 */
	public static Sprite getFloor() {
		return floor;
	}

	/**
	 * returns stair sprite
	 * 
	 * @return stair sprite
	 */
	public static Sprite getStair() {
		return stair;
	}

	/**
	 * returns boss sprite
	 * 
	 * @return boss sprite
	 */
	public static Sprite getBoss() {
		return boss;
	}

	/**
	 * returns mob sprite
	 * 
	 * @return mob sprite
	 */
	public static Sprite getMob() {
		return mob;
	}

	/**
	 * returns player sprite
	 * 
	 * @return player sprite
	 */
	public static Sprite getPlayer() {
		return player;
	}

	/**
	 * returns empty (black) sprite
	 * 
	 * @return empty (black) sprite
	 */
	public static Sprite getEmpty() {
		return empty;
	}

	/**
	 * Loads a sprite from the sprite-sheet
	 */
	private void load() {

		for (int y = 0; y < SIZE; y++) {
			for (int x = 0; x < SIZE; x++) {
				spritePixels[x + y * SIZE] = sheet.getSheetPixels()[(x + this.x) + (y + this.y) * sheet.SIZE];
			}
		}
	}

}