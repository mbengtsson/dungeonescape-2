package se.bengtsson.desc.graphics.sprites;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * 
 * Sprite-sheet class, loads a sprite-sheet
 * 
 * @author Marcus Bengtsson
 * 
 */
public class SpriteSheet {

	public final int SIZE;
	private String path;
	private int[] sheetPixels;

	private static SpriteSheet tileSheet = new SpriteSheet("/sprites/sprites.png", 256);

	/**
	 * Constructor, creates a sprite-sheet
	 * 
	 * @param path
	 *            to sprite-sheet
	 * @param size
	 *            of the sprite-sheet
	 */
	private SpriteSheet(String path, int size) {
		this.path = path;
		SIZE = size;
		sheetPixels = new int[SIZE * SIZE];
		load();
	}

	/**
	 * returns an array with the pixels of the sprite-sheet
	 * 
	 * @return an array with the pixels of the sprite-sheet
	 */
	public int[] getSheetPixels() {
		return sheetPixels;
	}

	/**
	 * Loads a sprite-sheet from the sprite-sheet image
	 */
	private void load() {
		try {
			BufferedImage image = ImageIO.read(this.getClass().getResource(path));
			int w = image.getWidth();
			int h = image.getHeight();
			image.getRGB(0, 0, w, h, sheetPixels, 0, w);

		} catch (IOException e) {

		}
	}

	/**
	 * returns the tiles sprite-sheet
	 * 
	 * @return the tiles sprite-sheet
	 */
	public static SpriteSheet getTileSheet() {
		return tileSheet;
	}
}