package se.bengtsson.desc.graphics;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.net.URL;

import javax.swing.ImageIcon;

/**
 * TileIcon, extends ImageIcon, tiles its image to fill the component area
 * 
 * @author Marcus Bengtsson
 * 
 */
public class TiledIcon extends ImageIcon {

	private static final long serialVersionUID = 1L;

	/**
	 * Creates an TiledIcon from an image object.
	 * 
	 * @param image
	 */
	public TiledIcon(Image image) {
		super(image);
	}

	/**
	 * Creates an TiledIcon from the specified file
	 * 
	 * @param filename
	 */
	public TiledIcon(String filename) {
		super(filename);
	}

	/**
	 * Creates an ImageIcon from the specified URL.
	 * 
	 * @param location
	 */
	public TiledIcon(URL location) {
		super(location);
	}

	@Override
	public void paintIcon(Component c, Graphics g, int x, int y) {

		Image image = getImage();

		// return if no image is loaded
		if (image == null) {
			return;
		}

		// get width and height of image
		int imageW = image.getWidth(c);
		int imageH = image.getHeight(c);

		// get the width and height of the container
		int width = c.getWidth();
		int height = c.getHeight();

		// paints the icon over the whole container
		for (y = 0; y < height; y += imageH) {
			for (x = 0; x < width; x += imageW) {
				super.paintIcon(c, g, x, y);
			}
		}

	}

}