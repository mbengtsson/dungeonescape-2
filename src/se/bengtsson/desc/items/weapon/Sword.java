package se.bengtsson.desc.items.weapon;

import java.util.ResourceBundle;

import se.bengtsson.desc.items.Item;
import se.bengtsson.desc.items.ItemStats;

/**
 * Sword class, creates swords
 * 
 * @author Marcus Bengtsson
 * 
 */
public class Sword extends Item {
	
	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("se.bengtsson.desc.properties.messages"); //$NON-NLS-1$

	/**
	 * Creates a new sword
	 */
	public Sword() {
		super(BUNDLE.getString("Item.weapon.sword.text"), ItemStats.SWORD.getWeight(), 1);

		setItemStat(ItemStats.SWORD.getStatValue());
	}

}
