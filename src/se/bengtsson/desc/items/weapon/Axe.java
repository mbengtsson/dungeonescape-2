package se.bengtsson.desc.items.weapon;

import java.util.ResourceBundle;

import se.bengtsson.desc.items.Item;
import se.bengtsson.desc.items.ItemStats;

/**
 * Axe class, creates axes
 * 
 * @author Marcus Bengtsson
 * 
 */
public class Axe extends Item {

	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("se.bengtsson.desc.properties.messages"); //$NON-NLS-1$

	/**
	 * Creates a new axe
	 */
	public Axe() {
		super(BUNDLE.getString("Item.weapon.axe.text"), ItemStats.AXE.getWeight(), 1);

		setItemStat(ItemStats.AXE.getStatValue());
	}

}
