package se.bengtsson.desc.items.weapon;

import java.util.ResourceBundle;

import se.bengtsson.desc.items.Item;
import se.bengtsson.desc.items.ItemStats;

/**
 * Dagger class, creates daggers
 * 
 * @author Marcus Bengtsson
 * 
 */
public class Dagger extends Item {
	
	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("se.bengtsson.desc.properties.messages"); //$NON-NLS-1$

	/**
	 * Creates a new dagger
	 */
	public Dagger() {
		super(BUNDLE.getString("Item.weapon.dagger.text"), ItemStats.DAGGER.getWeight(), 1);

		setItemStat(ItemStats.DAGGER.getStatValue());

	}

}
