package se.bengtsson.desc.items.weapon;

import java.util.ResourceBundle;

import se.bengtsson.desc.items.Item;
import se.bengtsson.desc.items.ItemStats;

/**
 * Club class, creates clubs
 * 
 * @author Marcus Bengtsson
 * 
 */
public class Club extends Item {
	
	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("se.bengtsson.desc.properties.messages"); //$NON-NLS-1$

	/**
	 * Creates a new club
	 */
	public Club() {
		super(BUNDLE.getString("Item.weapon.club.text"), ItemStats.CLUB.getWeight(), 1);

		setItemStat(ItemStats.CLUB.getStatValue());
	}

}
