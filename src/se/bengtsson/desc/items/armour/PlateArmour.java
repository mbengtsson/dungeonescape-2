package se.bengtsson.desc.items.armour;

import java.util.ResourceBundle;

import se.bengtsson.desc.items.Item;
import se.bengtsson.desc.items.ItemStats;

/**
 * Plate armour class, creates plate armour
 * 
 * @author Marcus Bengtsson
 * 
 */
public class PlateArmour extends Item {

	
	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("se.bengtsson.desc.properties.messages"); //$NON-NLS-1$

	/**
	 * Creates a new plate armour
	 */
	public PlateArmour() {
		super(BUNDLE.getString("Item.armour.plate.text"), ItemStats.PLATE_ARMOUR.getWeight(), 2);

		setItemStat(ItemStats.PLATE_ARMOUR.getStatValue());
	}

}
