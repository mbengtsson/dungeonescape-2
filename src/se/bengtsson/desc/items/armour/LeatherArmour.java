package se.bengtsson.desc.items.armour;

import java.util.ResourceBundle;

import se.bengtsson.desc.items.Item;
import se.bengtsson.desc.items.ItemStats;

/**
 * Leather armour class, creates leather armour
 * 
 * @author Marcus Bengtsson
 * 
 */
public class LeatherArmour extends Item {

	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("se.bengtsson.desc.properties.messages"); //$NON-NLS-1$
	
	/**
	 * Creates a new leather armour
	 */
	public LeatherArmour() {
		super(BUNDLE.getString("Item.armour.leather.text"), ItemStats.LEATHER_ARMOUR.getWeight(), 2);

		setItemStat(ItemStats.LEATHER_ARMOUR.getStatValue());
	}

}
