package se.bengtsson.desc.items.armour;

import java.util.ResourceBundle;

import se.bengtsson.desc.items.Item;
import se.bengtsson.desc.items.ItemStats;

/**
 * Mail armour class, creates mail armour
 * 
 * @author Marcus Bengtsson
 * 
 */
public class MailArmour extends Item {
	
	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("se.bengtsson.desc.properties.messages"); //$NON-NLS-1$

	/**
	 * Creates a new mail armour
	 */
	public MailArmour() {
		super(BUNDLE.getString("Item.armour.mail.text"), ItemStats.MAIL_ARMOUR.getWeight(), 2);

		setItemStat(ItemStats.MAIL_ARMOUR.getStatValue());
	}

}
