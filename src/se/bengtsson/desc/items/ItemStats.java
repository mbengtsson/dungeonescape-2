package se.bengtsson.desc.items;

/**
 * This enumeration contains stats for different items
 * 
 * @author Marcus Bengtsson
 * 
 */
public enum ItemStats {

	EMPTY(0, 0),
	
	DAGGER(1, 2), 
	CLUB(2, 4), 
	SWORD(2, 6), 
	AXE(5, 8),
	
	LEATHER_ARMOUR(2, 4), 
	MAIL_ARMOUR(4, 6), 
	PLATE_ARMOUR(6, 8);

	private int weight;
	private int statValue;

	private ItemStats(int weight, int statValue) {

		this.weight = weight;
		this.statValue = statValue;
	}

	/**
	 * Get item weight
	 * 
	 * @return weight
	 */
	public int getWeight() {
		return weight;
	}

	/**
	 * Get items stat value 
	 * 
	 * @return stat value
	 */
	public int getStatValue() {
		return statValue;
	}

}
