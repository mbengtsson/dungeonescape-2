package se.bengtsson.desc.items;

import java.util.ResourceBundle;

/**
 * Empty class, creates "empty items" that is representing an empty slot
 * 
 * @author Marcus Bengtsson
 * 
 */
public class Empty extends Item {
	
	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("se.bengtsson.desc.properties.messages"); //$NON-NLS-1$

	/**
	 * Creates a new empty item to represent nothing slotted
	 */
	public Empty() {
		super(BUNDLE.getString("Item.empty.empty.text"), ItemStats.EMPTY.getWeight(), 0);

		setItemStat(ItemStats.EMPTY.getStatValue());
	}

}
