package se.bengtsson.desc;

import java.awt.EventQueue;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import se.bengtsson.desc.gui.MainWindow;

/**
 * Dungeon Escape with GUI
 * 
 * exam for the course
 * "grafiska gränssintt och interaktion (graphical interface and interaction)" -
 * Teknikhögskolan 2013
 * 
 * by Marcus Bengtsson GBJU13
 * 
 * @author Marcus Bengtsson
 * 
 */
public class DungeonEscape {

	public static void main(String[] args) {

		// Sets look and feel to system default

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (UnsupportedLookAndFeelException e) {

		} catch (ClassNotFoundException e) {

		} catch (InstantiationException e) {

		} catch (IllegalAccessException e) {

		}

		// Starts the GUI

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					MainWindow.getInstance().showNewMainMenuView();
					MainWindow.getInstance().setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
