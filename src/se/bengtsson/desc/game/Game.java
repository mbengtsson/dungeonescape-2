package se.bengtsson.desc.game;

import java.util.Random;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import se.bengtsson.desc.creatures.player.Player;
import se.bengtsson.desc.dungeon.Maps;
import se.bengtsson.desc.gui.GameOverDialog;
import se.bengtsson.desc.gui.handlers.Keyboard;

/**
 * Game logic
 * 
 * @author Marcus Bengtsson
 * 
 */
public class Game {

	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("se.bengtsson.desc.properties.messages"); //$NON-NLS-1$

	private Player player;
	private Maps map;
	private Enemies enemies;
	private int area;
	private int turn = 0;
	private boolean classic = false;

	private GameState gameState;

	/**
	 * Constructor, setting up a game
	 * 
	 * @param name
	 *            player name
	 * @param area
	 *            area
	 * @param classic
	 *            flag if game is classic
	 * @param picPath
	 *            path to player picture
	 */
	public Game(String name, int area, boolean classic, String picPath) {
		this.area = area;
		this.classic = classic;
		map = new Maps(area);
		player = new Player(1, name, '@', picPath);

		player.setRandomLocation(map);
		map.addCreature(player);
		enemies = new Enemies(map);
		Random rnd = new Random();
		enemies.addEnemies(rnd.nextInt(10) + 10, rnd.nextInt(1) + 1, player.getLevel());

		gameState = new GameState();

		gameState.updateState(player, map, enemies, area, turn, classic);
	}

	/**
	 * One game-turn
	 * 
	 * @param action
	 *            players action for the game turn, defined in GameActions
	 */
	public void gameTurn(GameAction action) {

		int collision = 0;

		if (action.equals(GameAction.WAIT)) {

			if (player.getCurrentHp() < player.getTotalHp()) {
				player.setCurrentHp(player.getCurrentHp() + 1);
			}
			if (player.getCurrentPwr() < player.getTotalPwr()) {
				player.setCurrentPwr(player.getCurrentPwr() + 1);
			}

		} else {

			String direction = action.toString().toLowerCase();

			collision = player.move(map, direction);
		}

		if (collision == 0) {

		} else if (collision == 2) {

			Fight fight = new Fight(player, enemies.getEnemyAt(player.getX(), player.getY()));

			if (fight.startFight()) {
				player.loot(enemies.getEnemyAt(player.getX(), player.getY()));
				enemies.removeEnemy(enemies.getEnemyAt(player.getX(), player.getY()));
			} else {

				GameOverDialog god = new GameOverDialog();
				god.setVisible(true);

			}
		} else if (collision == 4) {
			if (enemies.size() == 0) {
				nextLevel();
			} else {
				JOptionPane.showMessageDialog(GameThread.getInstance().getGameArea(), String.format(BUNDLE.getString("Game.exitLocked.text"), enemies.size()),
						BUNDLE.getString("Game.exitLockedTitle.text"), JOptionPane.INFORMATION_MESSAGE);
				Keyboard key = (Keyboard) GameThread.getInstance().getGameArea().getKeyListeners()[0];
				key.clearKeyPressed();
			}
		}

		map.setArea(map.getArea());
		map.addCreature(player);

		enemies.moveEnemies(map, player);

		map.addCreature(player);

		turn++;

		gameState.updateState(player, map, enemies, area, turn, classic);
	}

	/**
	 * Loads next level
	 */
	public void nextLevel() {

		area++;

		map.setArea(area);

		player.setRandomLocation(map);
		enemies = new Enemies(map);
		Random rnd = new Random();
		enemies.addEnemies(rnd.nextInt(10) + 10, rnd.nextInt(1) + 1, player.getLevel());

		map.addCreature(player);

		JOptionPane.showMessageDialog(null, BUNDLE.getString("Game.newLevel.text") + area, BUNDLE.getString("Game.newLevelTitle.text"), JOptionPane.INFORMATION_MESSAGE);
		Keyboard key = (Keyboard) GameThread.getInstance().getGameArea().getKeyListeners()[0];
		key.clearKeyPressed();

	}

	/**
	 * Sets the game to a game-state
	 * 
	 * @param gameState
	 *            Game-state to apply
	 */
	public void setGameState(GameState gameState) {

		this.player = gameState.getPlayer();
		this.enemies = gameState.getEnemies();
		this.map = gameState.getMaps();
		this.area = gameState.getArea();
		this.turn = gameState.getTurn();
		this.classic = gameState.isClassic();

		this.gameState = gameState;

	}

	/**
	 * Returns the map object
	 * 
	 * @return map-opbject
	 */
	public Maps getMap() {
		return map;
	}

	/**
	 * returns the current state of the game as a GameState object
	 * 
	 * @return current GameState
	 */
	public GameState getGameState() {
		return gameState;
	}

	/**
	 * returns the player object
	 * 
	 * @return the player
	 */
	public Player getPlayer() {
		return player;
	}

}
