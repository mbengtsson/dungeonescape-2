package se.bengtsson.desc.game;

import se.bengtsson.desc.gui.MainWindow;
import se.bengtsson.desc.gui.views.components.GameArea;

/**
 * The game-thread singleton
 * 
 * @author Marcus Bengtsson
 * 
 */
public class GameThread implements Runnable {

	private Thread thread;
	private boolean running;
	private GameArea gameArea;

	private static GameThread instance;

	private GameThread() {

	}

	/**
	 * Returns an instance of the game thread
	 * 
	 * @return an instance of the game thread
	 */
	public synchronized static GameThread getInstance() {
		if (instance == null) {
			instance = new GameThread();
		}

		return instance;
	}

	/**
	 * Start method, starts the thread and sets running flag to true
	 */
	public synchronized void start() {
		if (!running) {
			running = true;
			thread = new Thread(this, "Game thread");
			thread.start();
		}
	}

	/**
	 * Stop method, stops the thread and sets running flag to false
	 */
	public synchronized void stop() {
		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Halt method, sets running flag to false but doesn't halt join the thread
	 */
	public synchronized void halt() {
		running = false;
	}

	/**
	 * Checks if thread is running
	 * 
	 * @return true if running, false if not
	 */
	public boolean isRunning() {
		return running;
	}

	/**
	 * Run method
	 */
	@Override
	public void run() {

		/**
		 * Shows the game-view
		 */
		MainWindow.getInstance().showNewGameView(gameArea);
		MainWindow.getInstance().setVisible(true);

		long lastTime = System.currentTimeMillis();

		while (running) {
			long now = System.currentTimeMillis();

			// runs update 60ish times per second (actual 1,02s)
			while (now - lastTime > 17) {
				gameArea.update();
				lastTime = now;
			}
			// renders as often it can
			gameArea.render();

		}
	}

	/**
	 * Returns the game area
	 * 
	 * @return the game area
	 */
	public GameArea getGameArea() {
		return gameArea;
	}

	/**
	 * Sets the game-area
	 * 
	 * @param gameArea
	 *            to set
	 */
	public void setGameArea(GameArea gameArea) {
		this.gameArea = gameArea;
	}

}
