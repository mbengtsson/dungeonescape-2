package se.bengtsson.desc.game;

import java.util.ArrayList;
import java.util.Random;

import se.bengtsson.desc.creatures.npc.Boss;
import se.bengtsson.desc.creatures.npc.Npc;
import se.bengtsson.desc.creatures.npc.TrashMob;
import se.bengtsson.desc.creatures.player.Player;
import se.bengtsson.desc.dungeon.Maps;
import se.bengtsson.desc.gui.GameOverDialog;
import se.bengtsson.desc.items.armour.LeatherArmour;
import se.bengtsson.desc.items.armour.MailArmour;
import se.bengtsson.desc.items.armour.PlateArmour;
import se.bengtsson.desc.items.weapon.Axe;
import se.bengtsson.desc.items.weapon.Club;
import se.bengtsson.desc.items.weapon.Dagger;
import se.bengtsson.desc.items.weapon.Sword;

/**
 * Enemies class that handles the enemies in the game
 * 
 * @author Marcus Bengtsson
 * 
 */
public class Enemies {

	private ArrayList<Npc> enemies;
	private Maps map;

	/**
	 * Constructor, sets map to add enemies on and creates an array-list to
	 * store them in
	 * 
	 * @param map
	 *            sets map to add enemies on
	 */
	public Enemies(Maps map) {
		this.map = map;
		enemies = new ArrayList<Npc>();
	}

	/**
	 * Adds npcs to the enemies-list
	 * 
	 * @param trash
	 *            number of thrash mobs
	 * @param bosses
	 *            number of bosses
	 * @param level
	 *            sets the level of the enemies
	 */
	public void addEnemies(int trash, int bosses, int level) {

		Random rnd = new Random();
		int rndItem;

		for (int i = 0; i < trash; i++) {

			TrashMob npc = new TrashMob(level, "Random mob");

			rndItem = rnd.nextInt(3);
			if (rndItem == 1) {
				npc.setWeapon(new Dagger());
			} else if (rndItem == 2) {
				npc.setWeapon(new Club());
			}

			rndItem = rnd.nextInt(3);
			if (rndItem == 1) {
				npc.setArmour(new LeatherArmour());
			} else if (rndItem == 2) {
				npc.setArmour(new MailArmour());
			}

			npc.setRandomLocation(map);
			enemies.add(npc);
			map.addCreature(npc);

		}

		for (int i = 0; i < bosses; i++) {

			Boss boss = new Boss(level, "Evil boss");

			rndItem = rnd.nextInt(3);
			if (rndItem == 0) {
				boss.setWeapon(new Club());
			} else if (rndItem == 1) {
				boss.setWeapon(new Sword());
			} else if (rndItem == 2) {
				boss.setWeapon(new Axe());
			}

			rndItem = rnd.nextInt(3);

			if (rndItem == 0) {
				boss.setArmour(new LeatherArmour());
			} else if (rndItem == 1) {
				boss.setArmour(new MailArmour());
			} else if (rndItem == 2) {
				boss.setArmour(new PlateArmour());
			}

			boss.setRandomLocation(map);
			enemies.add(boss);
			map.addCreature(boss);

		}
	}

	/**
	 * Moves all enemies on the the map
	 * 
	 * @param map
	 *            to move on
	 * @param player
	 *            to track
	 */
	public void moveEnemies(Maps map, Player player) {

		for (int i = 0; i < enemies.size(); i++) {
			if (getEnemy(i).moveNpc(map, player)) {
				Fight fight = new Fight(getEnemy(i), player);

				if (fight.startFight()) {

					GameOverDialog god = new GameOverDialog();
					god.setVisible(true);

				} else {
					player.loot(getEnemy(i));
					removeEnemy(getEnemy(i));
				}
			}

			map.addCreature(getEnemy(i));
		}

		if (player.levelUp()) {
			levelUp();
		}

	}

	/**
	 * Randomly selects npcs in the enemy-list and level them up
	 */
	public void levelUp() {
		Random rnd = new Random();

		for (Npc enemy : enemies) {
			int x = rnd.nextInt(2);
			if (x == 1) {
				enemy.levelUp();
			}
		}

	}

	/**
	 * Returns the size of the enemy list (ie. enemies left)
	 * 
	 * @return size of the enemy-list
	 */
	public int size() {
		return enemies.size();
	}

	/**
	 * Get enemy at index of the enemy-list
	 * 
	 * @param index
	 * @return the enemy, if there are no enemies it will returns null
	 */
	public Npc getEnemy(int index) {

		if (index < enemies.size()) {
			return enemies.get(index);
		}
		return null;
	}

	/**
	 * Get enemy from the list based on coordinate
	 * 
	 * @param x
	 *            x-coordinate
	 * @param y
	 *            y-coordinate
	 * @return the enemy, if there are no enemy at the coordinate it will return
	 *         null
	 */
	public Npc getEnemyAt(int x, int y) {

		for (Npc npc : enemies) {
			if (npc.getX() == x && npc.getY() == y) {
				return npc;
			}
		}

		return null;
	}

	/**
	 * Removes an npc if it is dead
	 * 
	 * @param enemy
	 *            enemy to remove
	 * @return true if removed, false if the npc is still alive
	 */
	public boolean removeEnemy(Npc enemy) {

		if (enemy.isAlive()) {
			return false;
		} else {
			enemies.remove(enemy);
			return true;
		}

	}

}
