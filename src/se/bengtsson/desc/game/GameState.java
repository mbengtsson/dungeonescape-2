package se.bengtsson.desc.game;

import se.bengtsson.desc.creatures.player.Player;
import se.bengtsson.desc.dungeon.Maps;

/**
 * GameState object, used to store a game-state
 * 
 * 
 * @author Marcus Bengtsson
 * 
 */
public class GameState {

	private Player player;
	private Maps maps;
	private Enemies enemies;

	private int area;
	private int turn;
	private boolean classic = false;

	/**
	 * Stores a gamestate
	 * 
	 * @param player
	 *            the player object
	 * @param maps
	 *            the map object
	 * @param enemies
	 *            the enemies object
	 * @param area
	 *            the active area number
	 * @param turn
	 *            the active turn number
	 * @param classic
	 *            the classic flag
	 */
	public void updateState(Player player, Maps maps, Enemies enemies, int area, int turn, boolean classic) {

		this.player = player;
		this.enemies = enemies;
		this.maps = maps;
		this.area = area;
		this.turn = turn;
		this.classic = classic;
	}

	/**
	 * returns the stored player
	 * 
	 * @return the stored player
	 */
	public Player getPlayer() {
		return player;
	}

	/**
	 * returns the stored enemies object
	 * 
	 * @return the stored enemies object
	 */
	public Enemies getEnemies() {
		return enemies;
	}

	/**
	 * returns the stored map object
	 * 
	 * @return the stored map object
	 */
	public Maps getMaps() {
		return maps;
	}

	/**
	 * returns the stored area number
	 * 
	 * @return the stored area number
	 */
	public int getArea() {
		return area;
	}

	/**
	 * returns the stored game turn
	 * 
	 * @return the stored game turn
	 */
	public int getTurn() {
		return turn;
	}

	/**
	 * returns the stored classic flag
	 * 
	 * @return the stored classic flag
	 */
	public boolean isClassic() {
		return classic;
	}

}
