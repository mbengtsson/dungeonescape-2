package se.bengtsson.desc.game;

import java.util.Random;
import java.util.ResourceBundle;

import se.bengtsson.desc.creatures.Creature;
import se.bengtsson.desc.gui.FightWindow;

/**
 * Fight class that handles fights between creatures
 * 
 * @author Marcus Bengtsson
 * 
 */
public class Fight {

	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("se.bengtsson.desc.properties.messages"); //$NON-NLS-1$

	private Creature attacker, defender;
	private FightWindow window;

	/**
	 * Constructor sets attacker and target and creates a fight window
	 * 
	 * @param attacker
	 *            the attacker
	 * @param defender
	 *            the target
	 */
	public Fight(Creature attacker, Creature defender) {

		this.attacker = attacker;
		this.defender = defender;

		window = new FightWindow(this.attacker, this.defender);
	}

	/**
	 * Starts a fight between the attacker and the target
	 * 
	 * @return returns true if attacker wins and false if target wins
	 */
	public boolean startFight() {

		window.addKeyListener(GameThread.getInstance().getGameArea().getKeyListeners()[0]);
		window.setAlwaysOnTop(true);
		window.setVisible(true);

		Random rnd = new Random();
		int damage, power;

		while (attacker.getCurrentHp() > 0) {

			try {
				Thread.sleep(800);
			} catch (InterruptedException e) {

			}

			damage = rnd.nextInt(attacker.getCurrentPwr());
			defender.setCurrentHp(defender.getCurrentHp() - damage);
			power = rnd.nextInt(damage / 2 + 1) + damage / 3;
			attacker.setCurrentPwr(attacker.getCurrentPwr() - power);
			if (damage < defender.getCurrentHp() / 4) {
				defender.setCurrentPwr(defender.getCurrentPwr() + rnd.nextInt(2) + 1);
				if (defender.getCurrentPwr() > defender.getTotalPwr()) {
					defender.setCurrentPwr(defender.getTotalPwr());
				}
			}

			// Fix to not show negative HP
			if (defender.getCurrentHp() < 0) {
				defender.setCurrentHp(0);
			}

			window.getDefenderHpBar().setValue(defender.getCurrentHp());
			window.getDefenderHealth().setText(BUNDLE.getString("FightWindow.lblHealth.text") + defender.getCurrentHp() + "/" + defender.getTotalHp());
			window.getAttackerPwrBar().setValue(attacker.getCurrentPwr());
			window.getAttackerPower().setText(BUNDLE.getString("FightWindow.lblPower.text") + attacker.getCurrentPwr() + "/" + attacker.getTotalPwr());
			window.getDefenderPwrBar().setValue(defender.getCurrentPwr());
			window.getDefenderPower().setText(BUNDLE.getString("FightWindow.lblPower.text") + defender.getCurrentPwr() + "/" + defender.getTotalPwr());

			try {
				Thread.sleep(800);
			} catch (InterruptedException e) {
				System.out.println("Interrupted");
			}

			if (defender.getCurrentHp() < 1) {

				attacker.addXP(defender);
				defender.kill();
				window.dispose();
				return true;
			}

			damage = rnd.nextInt(defender.getCurrentPwr());
			attacker.setCurrentHp(attacker.getCurrentHp() - damage);
			power = rnd.nextInt(damage / 2 + 1) + damage / 3;
			defender.setCurrentPwr(defender.getCurrentPwr() - power);
			if (damage < attacker.getCurrentHp() / 4) {
				attacker.setCurrentPwr(attacker.getCurrentPwr() + rnd.nextInt(2) + 1);
				if (attacker.getCurrentPwr() > attacker.getTotalPwr()) {
					attacker.setCurrentPwr(attacker.getTotalPwr());
				}
			}

			// Fix to not show negative HP
			if (attacker.getCurrentHp() < 0) {
				attacker.setCurrentHp(0);
			}

			window.getAttackerHpBar().setValue(attacker.getCurrentHp());
			window.getAttackerHealth().setText(BUNDLE.getString("FightWindow.lblHealth.text") + attacker.getCurrentHp() + "/" + attacker.getTotalHp());
			window.getDefenderPwrBar().setValue(defender.getCurrentPwr());
			window.getDefenderPower().setText(BUNDLE.getString("FightWindow.lblPower.text") + defender.getCurrentPwr() + "/" + defender.getTotalPwr());
			window.getAttackerPwrBar().setValue(attacker.getCurrentPwr());
			window.getAttackerPower().setText(BUNDLE.getString("FightWindow.lblPower.text") + attacker.getCurrentPwr() + "/" + attacker.getTotalPwr());

		}

		try {
			Thread.sleep(800);
		} catch (InterruptedException e) {

		}

		defender.addXP(attacker);
		attacker.kill();
		window.dispose();
		return false;

	}

}
