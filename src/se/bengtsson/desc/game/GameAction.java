package se.bengtsson.desc.game;

/**
 * Enumeration with commands for Game class
 * 
 * @author Marcus Bengtsson
 * 
 */
public enum GameAction {

	NORTH, EAST, WEST, SOUTH, WAIT;

}
