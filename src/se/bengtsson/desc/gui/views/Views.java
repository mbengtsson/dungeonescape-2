package se.bengtsson.desc.gui.views;

import javax.swing.JPanel;

/**
 * Abstract view class, the views for the main window extends this class
 * 
 * @author Marcus Bengtsson
 * 
 */
public abstract class Views extends JPanel {

	private static final long serialVersionUID = 343739843796151564L;
	protected JPanel view;

	/**
	 * Returns the views JPanel
	 * 
	 * @return the view JPanel
	 */
	public JPanel getPanel() {
		return view;
	}

}
