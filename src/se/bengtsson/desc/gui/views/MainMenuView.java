package se.bengtsson.desc.gui.views;

import java.awt.BorderLayout;
import java.awt.Dialog.ModalityType;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import se.bengtsson.desc.game.Game;
import se.bengtsson.desc.game.GameState;
import se.bengtsson.desc.game.GameThread;
import se.bengtsson.desc.graphics.TiledIcon;
import se.bengtsson.desc.gui.HelpDialog;
import se.bengtsson.desc.gui.NewGameDialog;
import se.bengtsson.desc.gui.views.components.GameArea;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.StreamException;
import com.thoughtworks.xstream.io.xml.DomDriver;

/**
 * The main menu view for the main window
 * 
 * @author Marcus Bengtsson
 * 
 */
public class MainMenuView extends Views {

	private static final long serialVersionUID = -4274426529425209166L;

	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("se.bengtsson.desc.properties.messages"); //$NON-NLS-1$

	private JLabel background;

	private JButton btnNewGame;
	private JButton btnContinue;
	private JButton btnLoad;
	private JButton btnDelete;
	private JButton btnHelp;
	private JButton btnQuit;

	/**
	 * Create the panel.
	 */
	public MainMenuView() {

		Handler handler = new Handler();

		view = new JPanel();
		view.setLayout(new BorderLayout(0, 0));

		background = new JLabel("");
		background.setIcon(new TiledIcon(this.getClass().getResource("/icons/bg.png")));
		view.add(background, BorderLayout.CENTER);
		background.setLayout(new BorderLayout(0, 0));

		JPanel titlePanel = new JPanel();
		titlePanel.setOpaque(false);
		titlePanel.setPreferredSize(new Dimension(10, 200));
		background.add(titlePanel, BorderLayout.NORTH);

		JLabel lblDungeonEscape = new JLabel("");
		lblDungeonEscape.setPreferredSize(new Dimension(600, 200));
		lblDungeonEscape.setIcon(new ImageIcon(this.getClass().getResource("/icons/logo.png")));
		titlePanel.add(lblDungeonEscape);

		JPanel centerPanel = new JPanel();
		centerPanel.setOpaque(false);
		background.add(centerPanel, BorderLayout.CENTER);

		JPanel buttonPanel = new JPanel();
		buttonPanel.setOpaque(false);
		centerPanel.add(buttonPanel);
		buttonPanel.setSize(new Dimension(150, 400));
		GridBagLayout gbl_buttonPanel = new GridBagLayout();
		gbl_buttonPanel.columnWidths = new int[] { 55, 0 };
		gbl_buttonPanel.rowHeights = new int[] { 0, 25, 0, 0, 0, 0, 25, 0 };
		gbl_buttonPanel.columnWeights = new double[] { 0.0, Double.MIN_VALUE };
		gbl_buttonPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		buttonPanel.setLayout(gbl_buttonPanel);

		btnNewGame = new JButton(BUNDLE.getString("MainMenuView.btnNewGame.text")); //$NON-NLS-1$
		btnNewGame.setPreferredSize(new Dimension(110, 25));
		GridBagConstraints gbc_btnNewGame = new GridBagConstraints();
		gbc_btnNewGame.insets = new Insets(0, 0, 5, 0);
		gbc_btnNewGame.gridx = 0;
		gbc_btnNewGame.gridy = 1;
		buttonPanel.add(btnNewGame, gbc_btnNewGame);

		btnNewGame.addActionListener(handler);

		btnContinue = new JButton(BUNDLE.getString("MainMenuView.btnContinue.text")); //$NON-NLS-1$
		btnContinue.setPreferredSize(new Dimension(110, 25));
		GridBagConstraints gbc_btnContinue = new GridBagConstraints();
		gbc_btnContinue.insets = new Insets(0, 0, 5, 0);
		gbc_btnContinue.gridx = 0;
		gbc_btnContinue.gridy = 2;
		buttonPanel.add(btnContinue, gbc_btnContinue);

		btnContinue.addActionListener(handler);

		btnLoad = new JButton(BUNDLE.getString("MainMenuView.btnLoad.text")); //$NON-NLS-1$
		btnLoad.setPreferredSize(new Dimension(110, 25));
		GridBagConstraints gbc_btnLoad = new GridBagConstraints();
		gbc_btnLoad.insets = new Insets(0, 0, 5, 0);
		gbc_btnLoad.gridx = 0;
		gbc_btnLoad.gridy = 3;
		buttonPanel.add(btnLoad, gbc_btnLoad);

		btnLoad.addActionListener(handler);

		btnDelete = new JButton(BUNDLE.getString("MainMenuView.btnDelete.text")); //$NON-NLS-1$
		btnDelete.setPreferredSize(new Dimension(110, 25));
		GridBagConstraints gbc_btnDelete = new GridBagConstraints();
		gbc_btnDelete.insets = new Insets(0, 0, 5, 0);
		gbc_btnDelete.gridx = 0;
		gbc_btnDelete.gridy = 4;
		buttonPanel.add(btnDelete, gbc_btnDelete);

		btnDelete.addActionListener(handler);

		btnHelp = new JButton(BUNDLE.getString("MainMenuView.btnHelp.text")); //$NON-NLS-1$
		btnHelp.setPreferredSize(new Dimension(110, 25));
		GridBagConstraints gbc_btnHelp = new GridBagConstraints();
		gbc_btnHelp.insets = new Insets(0, 0, 5, 0);
		gbc_btnHelp.gridx = 0;
		gbc_btnHelp.gridy = 5;
		buttonPanel.add(btnHelp, gbc_btnHelp);

		btnHelp.addActionListener(handler);

		btnQuit = new JButton(BUNDLE.getString("MainMenuView.btnQuit.text")); //$NON-NLS-1$
		btnQuit.setPreferredSize(new Dimension(110, 25));
		GridBagConstraints gbc_btnQuit = new GridBagConstraints();
		gbc_btnQuit.gridx = 0;
		gbc_btnQuit.gridy = 6;
		buttonPanel.add(btnQuit, gbc_btnQuit);

		btnQuit.addActionListener(handler);
	}

	private class Handler implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {

			if (e.getSource() == btnQuit) {

				int choise = JOptionPane.showConfirmDialog(null, BUNDLE.getString("MainMenuView.exitMessage.text"), BUNDLE.getString("MainMenuView.exitMessageTitle.text"),
						JOptionPane.OK_CANCEL_OPTION);
				if (choise == 0) {
					System.exit(0);
				}
			}

			if (e.getSource() == btnNewGame) {
				NewGameDialog newGame = new NewGameDialog();
				newGame.setModalityType(ModalityType.APPLICATION_MODAL);
				newGame.setVisible(true);

			}

			if (e.getSource() == btnHelp) {
				HelpDialog hw = new HelpDialog();
				hw.setModalityType(ModalityType.APPLICATION_MODAL);
				hw.setVisible(true);
			}

			if (e.getSource() == btnContinue) {

				XStream xstream = new XStream(new DomDriver());

				File path = new File("desc/saves/lastgame");

				if (path.exists()) {

					BufferedReader reader = null;

					try {
						reader = new BufferedReader(new FileReader(path));

						String save = "";
						String line;

						while (true) {
							line = reader.readLine();
							if (line == null) {
								break;
							}

							save += line;
						}

						GameState gameState = (GameState) xstream.fromXML(save);

						if (gameState.isClassic()) {
							path.delete();
						}
						
						File picPath = new File(gameState.getPlayer().getPicturePath());
						
						if (!picPath.exists()){
							BufferedImage image = ImageIO.read(this.getClass().getResource("/icons/player.png"));
							picPath = new File("desc/images/default.png");
							try {
								ImageIO.write(image, "png", picPath);

							} catch (IOException e1) {
								JOptionPane.showMessageDialog(null, BUNDLE.getString("FileIOError.text"), BUNDLE.getString("FileIOError.title.text"), JOptionPane.ERROR_MESSAGE);
							}
							gameState.getPlayer().setPicturePath(picPath.getPath());
							
						}

						GameThread gt = GameThread.getInstance();

						if (gt.isRunning()) {
							gt.stop();
						}

						Game game = new Game(gameState.getPlayer().getName(), gameState.getPlayer().getLevel(), gameState.isClassic(), gameState.getPlayer().getPicturePath());

						gt.setGameArea(new GameArea(game));

						gt.start();

						game.setGameState(gameState);

						gt.getGameArea().setPosition();

					} catch (FileNotFoundException e1) {
						JOptionPane.showMessageDialog(null, BUNDLE.getString("FileIOError.text"), BUNDLE.getString("FileIOError.title.text"), JOptionPane.ERROR_MESSAGE);
					} catch (IOException e1) {
						JOptionPane.showMessageDialog(null, BUNDLE.getString("FileIOError.text"), BUNDLE.getString("FileIOError.title.text"), JOptionPane.ERROR_MESSAGE);
					} finally {
						try {
							reader.close();
						} catch (IOException e1) {
							JOptionPane.showMessageDialog(null, BUNDLE.getString("FileIOError.text"), BUNDLE.getString("FileIOError.title.text"), JOptionPane.ERROR_MESSAGE);
						}
					}

				} else {
					JOptionPane.showMessageDialog(view, BUNDLE.getString("MainMenuView.noPreviousGame.text"), "", JOptionPane.ERROR_MESSAGE);
				}

			}

			if (e.getSource() == btnLoad) {

				File saves = new File("desc/saves");

				if (!saves.exists()) {
					saves.mkdirs();
				}

				JFileChooser fc = new JFileChooser();
				fc.setCurrentDirectory(saves);
				int choise = fc.showOpenDialog(view);

				if (fc.getSelectedFile() != null && choise == 0) {

					XStream xstream = new XStream(new DomDriver());

					BufferedReader reader = null;

					try {
						reader = new BufferedReader(new FileReader(fc.getSelectedFile()));

						String save = "";
						String line;

						while (true) {
							line = reader.readLine();
							if (line == null) {
								break;
							}

							save += line;
						}

						GameState gameState = (GameState) xstream.fromXML(save);

						if (gameState.isClassic()) {
							fc.getSelectedFile().delete();
						}
						
						File picPath = new File(gameState.getPlayer().getPicturePath());
						
						if (!picPath.exists()){
							BufferedImage image = ImageIO.read(this.getClass().getResource("/icons/player.png"));
							picPath = new File("desc/images/default.png");
							try {
								ImageIO.write(image, "png", picPath);

							} catch (IOException e1) {
								JOptionPane.showMessageDialog(null, BUNDLE.getString("FileIOError.text"), BUNDLE.getString("FileIOError.title.text"), JOptionPane.ERROR_MESSAGE);
							}
							gameState.getPlayer().setPicturePath(picPath.getPath());
							
						}
						

						GameThread gt = GameThread.getInstance();

						if (gt.isRunning()) {
							gt.stop();
						}

						Game game = new Game(gameState.getPlayer().getName(), gameState.getPlayer().getLevel(), gameState.isClassic(), gameState.getPlayer().getPicturePath());

						gt.setGameArea(new GameArea(game));

						gt.start();

						game.setGameState(gameState);
						gt.getGameArea().setPosition();

					} catch (StreamException e1) {
						JOptionPane.showMessageDialog(null, fc.getSelectedFile() + BUNDLE.getString("GameView.ErrorLoadingMessage.text"), BUNDLE.getString("GameView.ErrorLoadingMessageTitle.text"),
								JOptionPane.OK_OPTION);
					} catch (FileNotFoundException e1) {

					} catch (IOException e1) {
						JOptionPane.showMessageDialog(null, BUNDLE.getString("FileIOError.text"), BUNDLE.getString("FileIOError.title.text"), JOptionPane.ERROR_MESSAGE);
					} finally {
						try {
							reader.close();
						} catch (IOException e1) {
							JOptionPane.showMessageDialog(null, BUNDLE.getString("FileIOError.text"), BUNDLE.getString("FileIOError.title.text"), JOptionPane.ERROR_MESSAGE);
						}
					}

				}
			}

			if (e.getSource() == btnDelete) {

				File saves = new File("desc/saves");

				if (!saves.exists()) {
					saves.mkdirs();

				}

				JFileChooser fc = new JFileChooser();
				fc.setCurrentDirectory(saves);
				fc.setMultiSelectionEnabled(true);
				int choise = fc.showDialog(view, BUNDLE.getString("MainMenuView.btnDelete.text"));

				if (choise == 0 && fc.getSelectedFiles().length > 0) {

					if (JOptionPane.showConfirmDialog(view, String.format(BUNDLE.getString("MainMenuView.deleteFilesMessage.text"), fc.getSelectedFiles().length),
							BUNDLE.getString("MainMenuView.deleteFilesMessageTitle.text"), JOptionPane.YES_NO_OPTION) == 0) {

						for (File file : fc.getSelectedFiles()) {
							file.delete();
						}
					}
				}
			}

		}
	}
}
