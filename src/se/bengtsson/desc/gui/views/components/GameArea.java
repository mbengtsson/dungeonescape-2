package se.bengtsson.desc.gui.views.components;

import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

import se.bengtsson.desc.game.Game;
import se.bengtsson.desc.game.GameAction;
import se.bengtsson.desc.graphics.Render;
import se.bengtsson.desc.gui.MainWindow;
import se.bengtsson.desc.gui.handlers.Keyboard;

/**
 * Game area canvas, shows the game
 * 
 * @author Marcus Bengtsson
 * 
 */
public class GameArea extends Canvas {

	private static final long serialVersionUID = -5663935480904531933L;
	public static int width = 648;
	public static int height = 552;

	private Render render;

	private Keyboard key = new Keyboard();
	private boolean keyLock = false;

	private int xPos;
	private int yPos;

	private BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
	private int[] imagePixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();

	private Game game;

	/**
	 * Constructor
	 * 
	 * @param game
	 *            the active game
	 */
	public GameArea(Game game) {
		render = new Render(width, height);
		addKeyListener(key);
		this.game = game;

		setPosition();
	}

	/**
	 * Run each update "tick" in GameThread
	 */
	public void update() {

		if (!isFocusOwner()) {
			key.clearKeyPressed();
		}

		requestFocusInWindow();

		MainWindow.getInstance().getGameView().update(game.getGameState());

		if (key.isKeyPressed(KeyEvent.VK_UP) && yPos > -height / 2) {
			yPos -= 6;
		}
		if (key.isKeyPressed(KeyEvent.VK_DOWN) && yPos < game.getMap().HEIGHT * game.getMap().TILE_SIZE - height / 2) {
			yPos += 6;
		}
		if (key.isKeyPressed(KeyEvent.VK_LEFT) && xPos > -width / 2) {
			xPos -= 6;
		}
		if (key.isKeyPressed(KeyEvent.VK_RIGHT) && xPos < game.getMap().WIDTH * game.getMap().TILE_SIZE - width / 2) {
			xPos += 6;
		}
		if (key.isKeyPressed(KeyEvent.VK_SPACE) && yPos > -height / 2) {
			setPosition();
		}

		if (!key.isKeyPressed(KeyEvent.VK_W) && !key.isKeyPressed(KeyEvent.VK_S) && !key.isKeyPressed(KeyEvent.VK_A) && !key.isKeyPressed(KeyEvent.VK_D) && !key.isKeyPressed(KeyEvent.VK_R)) {

			keyLock = false;
		}

		if (!keyLock) {

			if (key.isKeyPressed(KeyEvent.VK_W)) {
				keyLock = true;
				game.gameTurn(GameAction.NORTH);
				setPosition();

			}
			if (key.isKeyPressed(KeyEvent.VK_S)) {
				keyLock = true;
				game.gameTurn(GameAction.SOUTH);
				setPosition();

			}
			if (key.isKeyPressed(KeyEvent.VK_A)) {
				keyLock = true;
				game.gameTurn(GameAction.WEST);
				setPosition();

			}
			if (key.isKeyPressed(KeyEvent.VK_D)) {
				keyLock = true;
				game.gameTurn(GameAction.EAST);
				setPosition();

			}
			if (key.isKeyPressed(KeyEvent.VK_R)) {
				keyLock = true;
				game.gameTurn(GameAction.WAIT);
				setPosition();

			}
		}
	}

	/**
	 * Centres the screen on the player
	 */
	public void setPosition() {
		xPos = game.getPlayer().getX() * game.getMap().TILE_SIZE - width / 2;
		yPos = game.getPlayer().getY() * game.getMap().TILE_SIZE - height / 2;
	}

	/**
	 * Run each render "tick" in GameThread
	 */
	public void render() {

		// sets the buffer strategy to double-buffering
		BufferStrategy bStrat = getBufferStrategy();

		if (bStrat == null) {
			createBufferStrategy(2);
			return;
		}

		// clears the renderer
		render.clear();

		// specifies which tiles of the map (64 * 64 tiles) that are visible on
		// the screen
		// x0,y0 are the top left tile and x1,y1 are the bottom right tile that
		// are visible and needs to be rendered.
		// adds one tile to the bottom and right to avoid "tiling" effect on
		// those sides, top and left are handled by the renderer class
		int x0 = xPos / game.getMap().TILE_SIZE;
		int y0 = yPos / game.getMap().TILE_SIZE;
		int x1 = (xPos + width + game.getMap().TILE_SIZE) / game.getMap().TILE_SIZE;
		int y1 = (yPos + height + game.getMap().TILE_SIZE) / game.getMap().TILE_SIZE;

		// loop through all the tiles that are visible on the screen and render
		// them
		for (int y = y0; y < y1; y++) {
			for (int x = x0; x < x1; x++) {

				// calculates the position on the screen for the tile
				int xp = x * game.getMap().getSprite(x, y).SIZE - xPos;
				int yp = y * game.getMap().getSprite(x, y).SIZE - yPos;

				render.renderTile(xp, yp, game.getMap().getSprite(x, y));

			}
		}

		// copies the rendered pixels to the image that is drawn to the screen
		for (int i = 0; i < imagePixels.length; i++) {
			imagePixels[i] = render.getRenderedPixels()[i];
		}

		// draws the the image to the graphic-buffer
		Graphics g = bStrat.getDrawGraphics();
		g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
		g.dispose();
		// show the next ready image from the graphic-buffer
		bStrat.show();
	}

	/**
	 * Returns the active game
	 * 
	 * @return the active game
	 */
	public Game getGame() {
		return game;
	}

}
