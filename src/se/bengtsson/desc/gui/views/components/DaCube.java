package se.bengtsson.desc.gui.views.components;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;

/**
 * Draws a rotating cube that follows the mouse cursor on a JPanel
 * 
 * @author Marcus Bengtsson
 * 
 */
public class DaCube extends JPanel implements Runnable, MouseMotionListener {

	private static final long serialVersionUID = 7371729418709216396L;

	private int[][] cube;
	private int size = 200;

	private Thread thread;
	private boolean running = false;

	private int xPos, yPos;
	private int red, green, blue;

	private double xAngle, yAngle, zAngle, xRot, yRot;

	/**
	 * Constructor
	 */
	public DaCube() {

		cube = getCube(size);
		addMouseMotionListener(this);

		setBackground(new Color(0, 0, 0, 0));

		xRot = 2.0;
		yRot = 3.0;

		red = 77;
		green = 11;
		blue = 7;

		start();

	}

	/**
	 * Start method, start the thread and sets running to true
	 */
	private synchronized void start() {

		if (!running) {
			running = true;
			thread = new Thread(this, "DaCube");
			thread.start();
		}
	}

	/**
	 * Stop method, stops the tread a snd sets running to false
	 */
	public synchronized void stop() {
		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Overides paint, draws the cube on the screen and anything else on top of
	 * it, this panel needs to be transparent to see the cube
	 */
	@Override
	public void paint(Graphics g) {

		Graphics2D g2d = (Graphics2D) g;
		g.clearRect(0, 0, getWidth(), getHeight());

		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		drawCube(g2d, rotate(xAngle, yAngle, zAngle), xPos, yPos);

		super.paint(g);

		g.dispose();

	}

	/**
	 * Generates the cube of a specified size and return the coordinates of its
	 * corners
	 * 
	 * @param size
	 *            of the cube
	 * @return an 2d int-array with the coordinates of its corners
	 */
	public int[][] getCube(int size) {

		int x0 = 0;
		int y0 = 0;
		int z0 = 0;
		int x1 = size;
		int y1 = size;
		int z1 = size;

		int corners[][] = { { x0, y0, z0 }, { x1, y0, y0 }, { x0, y1, z0 }, { x1, y1, z0 }, { x0, y0, z1 }, { x1, y0, y1 }, { x0, y1, z1 }, { x1, y1, z1 } };

		return corners;
	}

	/**
	 * 
	 * Rotates the cube by an angle
	 * 
	 * @param angleX
	 *            x-rotation
	 * @param angleY
	 *            y-rotation
	 * @param angleZ
	 *            z-rotation
	 * @return an 2d int-array with the new coordinates of the cubes corners
	 */
	private int[][] rotate(double angleX, double angleY, double angleZ) {

		// converting angles to radians
		angleZ *= (Math.PI / 180);
		angleY *= (Math.PI / 180);
		angleX *= (Math.PI / 180);

		int[][] rotCube = new int[8][3];

		for (int c = 0; c < cube.length; c++) {

			// coordinates relative to the centre of the cube
			// because the cube should rotate around its centre
			double xr = cube[c][0] - size / 2;
			double yr = cube[c][1] - size / 2;
			double zr = cube[c][2] - size / 2;

			// rotation around z-axis
			double zx = xr * Math.cos(angleZ) - yr * Math.sin(angleZ) - xr;
			double zy = xr * Math.sin(angleZ) + yr * Math.cos(angleZ) - yr;

			// rotation around y-axis
			double yx = (xr + zx) * Math.cos(angleY) - zr * Math.sin(angleY) - (xr + zx);
			double yz = (xr + zx) * Math.sin(angleY) + zr * Math.cos(angleY) - zr;

			// rotation around x-axis
			double xy = (yr + zy) * Math.cos(angleX) - (zr + yz) * Math.sin(angleX) - (yr + zy);
			double xz = (yr + zy) * Math.sin(angleX) + (zr + yz) * Math.cos(angleX) - (zr + yz);

			// offset for the new coordinates (+0.5 for proper
			// rounding)
			int xOffset = (int) (yx + zx + 0.5);
			int yOffset = (int) (zy + xy + 0.5);
			int zOffset = (int) (xz + yz + 0.5);

			// places character in space at the new coordinates
			// based on rotation

			rotCube[c][0] = cube[c][0] + xOffset;
			rotCube[c][1] = cube[c][1] + yOffset;
			rotCube[c][2] = cube[c][2] + zOffset;

		}
		return rotCube;
	}

	/**
	 * draws the cube on the panel
	 * 
	 * @param g2d
	 *            A Graphics2D object
	 * @param cube
	 *            the cube
	 * @param xPos
	 *            x-position on the screen
	 * @param yPos
	 *            y-position on the screen
	 */
	private void drawCube(Graphics2D g2d, int[][] cube, int xPos, int yPos) {

		g2d.setStroke(new BasicStroke(10, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));

		g2d.setColor(new Color(red, green, blue));

		g2d.drawLine(xPos + cube[0][0], yPos + cube[0][1], xPos + cube[1][0], yPos + cube[1][1]);
		g2d.drawLine(xPos + cube[2][0], yPos + cube[2][1], xPos + cube[3][0], yPos + cube[3][1]);
		g2d.drawLine(xPos + cube[0][0], yPos + cube[0][1], xPos + cube[2][0], yPos + cube[2][1]);
		g2d.drawLine(xPos + cube[1][0], yPos + cube[1][1], xPos + cube[3][0], yPos + cube[3][1]);

		g2d.drawLine(xPos + cube[4][0], yPos + cube[4][1], xPos + cube[5][0], yPos + cube[5][1]);
		g2d.drawLine(xPos + cube[6][0], yPos + cube[6][1], xPos + cube[7][0], yPos + cube[7][1]);
		g2d.drawLine(xPos + cube[4][0], yPos + cube[4][1], xPos + cube[6][0], yPos + cube[6][1]);
		g2d.drawLine(xPos + cube[5][0], yPos + cube[5][1], xPos + cube[7][0], yPos + cube[7][1]);

		g2d.drawLine(xPos + cube[0][0], yPos + cube[0][1], xPos + cube[4][0], yPos + cube[4][1]);
		g2d.drawLine(xPos + cube[1][0], yPos + cube[1][1], xPos + cube[5][0], yPos + cube[5][1]);
		g2d.drawLine(xPos + cube[2][0], yPos + cube[2][1], xPos + cube[6][0], yPos + cube[6][1]);
		g2d.drawLine(xPos + cube[3][0], yPos + cube[3][1], xPos + cube[7][0], yPos + cube[7][1]);

		g2d.setStroke(new BasicStroke(1));

	}

	@Override
	public void run() {

		while (running) {

			xAngle += xRot;
			yAngle += yRot;
			zAngle++;

			red = (int) Math.abs((Math.sin(xAngle / 100) * 180));
			green = (int) Math.abs((Math.cos(yAngle / 100) * 180));
			blue = (int) Math.abs((Math.sin(zAngle / 200) * 180));

			repaint();

			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {

			}
		}

	}

	/**
	 * Change the rotation of the cube when the mouse is dragged
	 */
	@Override
	public void mouseDragged(MouseEvent e) {
		xRot = (xPos - e.getX()) / 50;
		yRot = (yPos - e.getY()) / 50;
	}

	/**
	 * The cube follows the mouse-cursor
	 */
	@Override
	public void mouseMoved(MouseEvent e) {
		xPos = e.getX() - size / 2;
		yPos = e.getY() - size / 2;
	}

}
