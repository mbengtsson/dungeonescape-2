package se.bengtsson.desc.gui.views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog.ModalityType;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;

import se.bengtsson.desc.game.GameState;
import se.bengtsson.desc.game.GameThread;
import se.bengtsson.desc.graphics.TiledIcon;
import se.bengtsson.desc.gui.AboutDialog;
import se.bengtsson.desc.gui.HelpDialog;
import se.bengtsson.desc.gui.InventoryWindow;
import se.bengtsson.desc.gui.MainWindow;
import se.bengtsson.desc.gui.NewGameDialog;
import se.bengtsson.desc.gui.views.components.GameArea;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.StreamException;
import com.thoughtworks.xstream.io.xml.DomDriver;

import java.util.ResourceBundle;

/**
 * The game view for the main window
 * 
 * @author Marcus Bengtsson
 * 
 */
public class GameView extends Views {

	private static final long serialVersionUID = 6205656963214167149L;

	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("se.bengtsson.desc.properties.messages"); //$NON-NLS-1$

	private GameArea gameArea;
	private JLabel lblTurn;
	private JLabel lblName;
	private JLabel lblLevelN;
	private JLabel lblXpNm;
	private JLabel lblHpNbm;
	private JLabel lblPwrNbm;
	private JLabel lblArmour;
	private JLabel lblWeapon;
	private JLabel lblAreaN;
	private JLabel lblEnemiesLeftN;
	private JButton btnLoad;
	private JButton btnSave;
	private JButton btnHelp;
	private JButton btnInventory;
	private JMenuItem mntmSaveGame;
	private JMenuItem mntmExit;
	private JMenuItem mntmNewGame;
	private JMenuItem mntmLoadGame;
	private JMenuItem mntmInventory;
	private JMenuItem mntmHelp;
	private JMenuItem mntmAbout;
	private JLabel lblPlayerPic;

	public GameView(GameArea gameArea) {

		this.gameArea = gameArea;

		Handler handler = new Handler();

		view = new JPanel();
		view.setPreferredSize(new Dimension(800, 600));
		view.setLayout(new BorderLayout(0, 0));

		JLabel background = new JLabel("");
		background.setIcon(new TiledIcon(this.getClass().getResource("/icons/bg.png")));
		view.add(background, BorderLayout.CENTER);
		background.setLayout(new BorderLayout(0, 0));

		// MENU BAR

		JMenuBar menuBar = new JMenuBar();
		background.add(menuBar, BorderLayout.NORTH);

		JMenu fileMenu = new JMenu(BUNDLE.getString("GameView.fileMenu.text")); //$NON-NLS-1$
		menuBar.add(fileMenu);

		mntmNewGame = new JMenuItem(BUNDLE.getString("GameView.mntmNewGame.text")); //$NON-NLS-1$
		fileMenu.add(mntmNewGame);
		mntmNewGame.addActionListener(handler);

		fileMenu.addSeparator();

		mntmLoadGame = new JMenuItem(BUNDLE.getString("GameView.mntmLoadGame.text")); //$NON-NLS-1$
		fileMenu.add(mntmLoadGame);
		mntmLoadGame.addActionListener(handler);

		mntmSaveGame = new JMenuItem(BUNDLE.getString("GameView.mntmSaveGame.text")); //$NON-NLS-1$
		fileMenu.add(mntmSaveGame);
		mntmSaveGame.addActionListener(handler);

		fileMenu.addSeparator();

		mntmExit = new JMenuItem(BUNDLE.getString("GameView.mntmExit.text")); //$NON-NLS-1$
		fileMenu.add(mntmExit);
		mntmExit.addActionListener(handler);

		JMenu viewMenu = new JMenu(BUNDLE.getString("GameView.viewMenu.text")); //$NON-NLS-1$
		menuBar.add(viewMenu);

		mntmInventory = new JMenuItem(BUNDLE.getString("GameView.mntmInventory.text")); //$NON-NLS-1$
		viewMenu.add(mntmInventory);
		mntmInventory.addActionListener(handler);

		JMenu helpMenu = new JMenu(BUNDLE.getString("GameView.helpMenu.text")); //$NON-NLS-1$
		menuBar.add(helpMenu);

		mntmHelp = new JMenuItem(BUNDLE.getString("GameView.mntmHelp.text")); //$NON-NLS-1$
		helpMenu.add(mntmHelp);
		mntmHelp.addActionListener(handler);

		helpMenu.addSeparator();

		mntmAbout = new JMenuItem(BUNDLE.getString("GameView.mntmAbout.text")); //$NON-NLS-1$
		helpMenu.add(mntmAbout);
		mntmAbout.addActionListener(handler);

		// END OF MENU BAR

		JPanel sideArea = new JPanel();
		sideArea.setOpaque(false);

		background.add(sideArea, BorderLayout.EAST);
		sideArea.setLayout(new BorderLayout(0, 0));

		JPanel topPanel = new JPanel();
		topPanel.setOpaque(false);
		sideArea.add(topPanel, BorderLayout.NORTH);

		lblTurn = new JLabel(BUNDLE.getString("GameView.lblTurn.text")); //$NON-NLS-1$
		lblTurn.setOpaque(true);
		lblTurn.setPreferredSize(new Dimension(140, 15));
		topPanel.add(lblTurn);
		lblTurn.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		lblTurn.setHorizontalAlignment(SwingConstants.CENTER);

		JPanel middlePanel = new JPanel();
		middlePanel.setOpaque(false);
		middlePanel.setMaximumSize(new Dimension(150, 32767));
		middlePanel.setPreferredSize(new Dimension(150, 10));
		sideArea.add(middlePanel, BorderLayout.CENTER);
		middlePanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		lblPlayerPic = new JLabel("");
		lblPlayerPic.setIcon(new ImageIcon(this.getClass().getResource("/icons/player.png")));
		middlePanel.add(lblPlayerPic);

		JPanel playerPanel = new JPanel();
		playerPanel.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		playerPanel.setPreferredSize(new Dimension(140, 115));
		middlePanel.add(playerPanel);
		playerPanel.setLayout(null);

		lblName = new JLabel(BUNDLE.getString("GameView.lblName.text")); //$NON-NLS-1$
		lblName.setFont(new Font("Dialog", Font.BOLD, 12));
		lblName.setBounds(5, 12, 135, 15);
		playerPanel.add(lblName);

		lblLevelN = new JLabel(BUNDLE.getString("GameView.lblLevelN.text")); //$NON-NLS-1$
		lblLevelN.setBounds(5, 29, 135, 15);
		playerPanel.add(lblLevelN);

		lblXpNm = new JLabel(BUNDLE.getString("GameView.lblXpNm.text")); //$NON-NLS-1$
		lblXpNm.setBounds(5, 46, 135, 15);
		playerPanel.add(lblXpNm);

		lblHpNbm = new JLabel(BUNDLE.getString("GameView.lblHpNbm.text")); //$NON-NLS-1$
		lblHpNbm.setBounds(5, 73, 135, 15);
		playerPanel.add(lblHpNbm);

		lblPwrNbm = new JLabel(BUNDLE.getString("GameView.lblPwrNbm.text")); //$NON-NLS-1$
		lblPwrNbm.setBounds(5, 88, 135, 15);
		playerPanel.add(lblPwrNbm);

		JPanel equipmentPanel = new JPanel();
		equipmentPanel.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		equipmentPanel.setPreferredSize(new Dimension(140, 85));
		middlePanel.add(equipmentPanel);
		equipmentPanel.setLayout(null);

		JLabel lblArmour_1 = new JLabel(BUNDLE.getString("GameView.lblArmour_1.text")); //$NON-NLS-1$
		lblArmour_1.setBounds(5, 12, 70, 15);
		equipmentPanel.add(lblArmour_1);

		lblArmour = new JLabel(BUNDLE.getString("GameView.lblArmour.text")); //$NON-NLS-1$
		lblArmour.setBounds(5, 27, 135, 15);
		equipmentPanel.add(lblArmour);

		JLabel lblWeapon_1 = new JLabel(BUNDLE.getString("GameView.lblWeapon_1.text")); //$NON-NLS-1$
		lblWeapon_1.setBounds(5, 42, 70, 15);
		equipmentPanel.add(lblWeapon_1);

		lblWeapon = new JLabel(BUNDLE.getString("GameView.lblWeapon.text")); //$NON-NLS-1$
		lblWeapon.setBounds(5, 57, 135, 15);
		equipmentPanel.add(lblWeapon);

		JPanel bottomPanel = new JPanel();
		bottomPanel.setOpaque(false);
		bottomPanel.setPreferredSize(new Dimension(150, 130));
		bottomPanel.setMaximumSize(new Dimension(150, 32767));
		sideArea.add(bottomPanel, BorderLayout.SOUTH);

		JPanel areaPanel = new JPanel();
		areaPanel.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		areaPanel.setPreferredSize(new Dimension(140, 55));
		bottomPanel.add(areaPanel);
		areaPanel.setLayout(null);

		lblAreaN = new JLabel(BUNDLE.getString("GameView.lblAreaN.text")); //$NON-NLS-1$
		lblAreaN.setBackground(new Color(238, 238, 238, 80));
		lblAreaN.setBounds(5, 12, 135, 15);
		areaPanel.add(lblAreaN);

		lblEnemiesLeftN = new JLabel(BUNDLE.getString("GameView.lblEnemiesLeftN.text")); //$NON-NLS-1$
		lblEnemiesLeftN.setBounds(5, 28, 135, 15);
		areaPanel.add(lblEnemiesLeftN);

		btnInventory = new JButton(BUNDLE.getString("GameView.btnInventory.text")); //$NON-NLS-1$
		btnInventory.setPreferredSize(new Dimension(70, 25));
		bottomPanel.add(btnInventory);
		btnInventory.addActionListener(handler);

		btnHelp = new JButton(BUNDLE.getString("GameView.btnHelp.text")); //$NON-NLS-1$
		btnHelp.setPreferredSize(new Dimension(70, 25));
		bottomPanel.add(btnHelp);
		btnHelp.addActionListener(handler);

		btnLoad = new JButton(BUNDLE.getString("GameView.btnLoad.text")); //$NON-NLS-1$
		btnLoad.setPreferredSize(new Dimension(70, 25));
		bottomPanel.add(btnLoad);
		btnLoad.addActionListener(handler);

		btnSave = new JButton(BUNDLE.getString("GameView.btnSave.text")); //$NON-NLS-1$
		btnSave.setPreferredSize(new Dimension(70, 25));
		bottomPanel.add(btnSave);
		btnSave.addActionListener(handler);

		JPanel centerArea = new JPanel();
		centerArea.setBackground(Color.BLACK);
		background.add(centerArea, BorderLayout.CENTER);
		centerArea.setLayout(new BorderLayout(0, 0));

		centerArea.add(gameArea, BorderLayout.CENTER);

	}

	/**
	 * Updates the game-view
	 * 
	 * @param gameState
	 *            game state with data to update
	 */
	public void update(GameState gameState) {

		lblTurn.setText(BUNDLE.getString("GameView.lblTurn.text") + gameState.getTurn());

		lblPlayerPic.setIcon(new ImageIcon(gameState.getPlayer().getPicturePath()));

		lblName.setText(gameState.getPlayer().getName());
		lblLevelN.setText(BUNDLE.getString("GameView.lblLevelN.text") + gameState.getPlayer().getLevel());

		lblXpNm.setText(BUNDLE.getString("GameView.lblXpNm.text") + gameState.getPlayer().getXp() + " (" + gameState.getPlayer().getLevel() * gameState.getPlayer().getLevel() * 5 + ")");
		lblHpNbm.setText(BUNDLE.getString("GameView.lblHpNbm.text") + gameState.getPlayer().getCurrentHp() + " (" + gameState.getPlayer().getHp() + "+"
				+ gameState.getPlayer().getArmour().getItemStat() + ")");
		lblPwrNbm.setText(BUNDLE.getString("GameView.lblPwrNbm.text") + gameState.getPlayer().getCurrentPwr() + " (" + gameState.getPlayer().getPwr() + "+"
				+ gameState.getPlayer().getWeapon().getItemStat() + ")");

		lblArmour.setText(gameState.getPlayer().getArmour().getName() + " +" + gameState.getPlayer().getArmour().getItemStat() + " " + BUNDLE.getString("GameView.lblHpNbm.text"));
		lblWeapon.setText(gameState.getPlayer().getWeapon().getName() + " +" + gameState.getPlayer().getWeapon().getItemStat() + " " + BUNDLE.getString("GameView.lblPwrNbm.text"));

		lblAreaN.setText(BUNDLE.getString("GameView.lblAreaN.text") + gameState.getArea());
		lblEnemiesLeftN.setText(BUNDLE.getString("GameView.lblEnemiesLeftN.text") + gameState.getEnemies().size());

		if (gameState.isClassic()) {
			btnSave.setEnabled(false);
			mntmSaveGame.setEnabled(false);
		} else {
			btnSave.setEnabled(true);
			mntmSaveGame.setEnabled(true);
		}
	}

	private class Handler implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {

			// if player choose exit in the menu
			if (e.getSource() == mntmExit) {

				int choise = JOptionPane.showConfirmDialog(null, BUNDLE.getString("GameView.exitMessage.text"), BUNDLE.getString("GameView.exitMessageTitle.text"), JOptionPane.OK_CANCEL_OPTION);
				if (choise == 0) {

					GameState gameState = gameArea.getGame().getGameState();

					File saves = new File("desc/saves");

					if (!saves.exists()) {
						saves.mkdirs();
					}

					XStream xstream = new XStream(new DomDriver());
					String save = xstream.toXML(gameState);

					BufferedWriter writer = null;

					try {

						if (gameState.isClassic()) {

							String name = gameState.getPlayer().getName().toLowerCase();
							writer = new BufferedWriter(new FileWriter("desc/saves/" + name + ".classic"));

						} else {
							writer = new BufferedWriter(new FileWriter("desc/saves/lastgame"));
						}

						writer.write(save);

					} catch (IOException e1) {
						JOptionPane.showMessageDialog(null, BUNDLE.getString("FileIOError.text"), BUNDLE.getString("FileIOError.title.text"), JOptionPane.ERROR_MESSAGE);
					} finally {
						try {
							writer.close();
						} catch (IOException e1) {
							JOptionPane.showMessageDialog(null, BUNDLE.getString("FileIOError.text"), BUNDLE.getString("FileIOError.title.text"), JOptionPane.ERROR_MESSAGE);
						}
					}

					GameThread.getInstance().stop();

					MainWindow.getInstance().showNewMainMenuView();
					MainWindow.getInstance().setVisible(true);

				}

			}

			// if player choose new game in the menu
			if (e.getSource() == mntmNewGame) {
				NewGameDialog newGame = new NewGameDialog();
				newGame.setModalityType(ModalityType.APPLICATION_MODAL);
				newGame.setVisible(true);

			}

			// if player choose inventory in the menu or clicks the inventory
			// button
			if (e.getSource() == mntmInventory || e.getSource() == btnInventory) {
				InventoryWindow invWindow = new InventoryWindow(gameArea.getGame().getPlayer());
				invWindow.setModalityType(ModalityType.APPLICATION_MODAL);
				invWindow.setVisible(true);

			}

			// if player choose help in the menu or clicks the help button
			if (e.getSource() == mntmHelp || e.getSource() == btnHelp) {
				HelpDialog hw = new HelpDialog();
				hw.setModalityType(ModalityType.APPLICATION_MODAL);
				hw.setVisible(true);
			}

			// if player choose save game in the menu or clicks the sav button
			if (e.getSource() == mntmSaveGame || e.getSource() == btnSave) {
				GameState gameState = gameArea.getGame().getGameState();

				File saves = new File("desc/saves");

				if (!saves.exists()) {
					saves.mkdirs();
				}

				JFileChooser fc = new JFileChooser();
				fc.setCurrentDirectory(saves);
				int choise = fc.showSaveDialog(view);

				if (fc.getSelectedFile() != null && choise == 0) {

					XStream xstream = new XStream(new DomDriver());
					String save = xstream.toXML(gameState);

					BufferedWriter writer = null;

					try {
						writer = new BufferedWriter(new FileWriter(fc.getSelectedFile()));
						writer.write(save);

					} catch (IOException e1) {
						JOptionPane.showMessageDialog(null, BUNDLE.getString("FileIOError.text"), BUNDLE.getString("FileIOError.title.text"), JOptionPane.ERROR_MESSAGE);
					} finally {
						try {
							writer.close();
						} catch (IOException e1) {
							JOptionPane.showMessageDialog(null, BUNDLE.getString("FileIOError.text"), BUNDLE.getString("FileIOError.title.text"), JOptionPane.ERROR_MESSAGE);
						}
					}
				}
			}

			// if player choose load game in the menu or clicks the load button
			if (e.getSource() == mntmLoadGame || e.getSource() == btnLoad) {

				File saves = new File("desc/saves");

				if (!saves.exists()) {
					saves.mkdirs();
				}

				JFileChooser fc = new JFileChooser();
				fc.setCurrentDirectory(saves);
				int choise = fc.showOpenDialog(view);

				if (fc.getSelectedFile() != null && choise == 0) {

					XStream xstream = new XStream(new DomDriver());

					BufferedReader reader = null;

					try {
						reader = new BufferedReader(new FileReader(fc.getSelectedFile()));

						String save = "";
						String line;

						while (true) {
							line = reader.readLine();
							if (line == null) {
								break;
							}

							save += line;
						}

						GameState gameState = null;

						gameState = (GameState) xstream.fromXML(save);

						if (gameState.isClassic()) {
							fc.getSelectedFile().delete();
						}
						
						File picPath = new File(gameState.getPlayer().getPicturePath());
						
						if (!picPath.exists()){
							BufferedImage image = ImageIO.read(this.getClass().getResource("/icons/player.png"));
							picPath = new File("desc/images/default.png");
							try {
								ImageIO.write(image, "png", picPath);

							} catch (IOException e1) {
								JOptionPane.showMessageDialog(null, BUNDLE.getString("FileIOError.text"), BUNDLE.getString("FileIOError.title.text"), JOptionPane.ERROR_MESSAGE);
							}
							gameState.getPlayer().setPicturePath(picPath.getPath());
							
						}

						gameArea.getGame().setGameState(gameState);
						gameArea.setPosition();

					} catch (StreamException e1) {
						JOptionPane.showMessageDialog(null, fc.getSelectedFile() + BUNDLE.getString("GameView.ErrorLoadingMessage.text"), BUNDLE.getString("GameView.ErrorLoadingMessageTitle.text"),
								JOptionPane.ERROR_MESSAGE);
					} catch (FileNotFoundException e1) {
						JOptionPane.showMessageDialog(null, BUNDLE.getString("FileIOError.text"), BUNDLE.getString("FileIOError.title.text"), JOptionPane.ERROR_MESSAGE);
					} catch (IOException e1) {
						JOptionPane.showMessageDialog(null, BUNDLE.getString("FileIOError.text"), BUNDLE.getString("FileIOError.title.text"), JOptionPane.ERROR_MESSAGE);
					} finally {
						try {
							reader.close();
						} catch (IOException e1) {
							JOptionPane.showMessageDialog(null, BUNDLE.getString("FileIOError.text"), BUNDLE.getString("FileIOError.title.text"), JOptionPane.ERROR_MESSAGE);
						}
					}

				}
			}

			// if the player choose about in the menu
			if (e.getSource() == mntmAbout) {
				AboutDialog ad = new AboutDialog();
				ad.setModalityType(ModalityType.APPLICATION_MODAL);
				ad.setVisible(true);
			}
		}

	}
}
