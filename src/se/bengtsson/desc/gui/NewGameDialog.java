package se.bengtsson.desc.gui;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutionException;

import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingWorker;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.imgscalr.Scalr;

import se.bengtsson.desc.game.Game;
import se.bengtsson.desc.game.GameThread;
import se.bengtsson.desc.gui.views.components.GameArea;

/**
 * New game dialog
 * 
 * @author Marcus Bengtsson
 * 
 */
public class NewGameDialog extends JDialog {

	private static final long serialVersionUID = -922093914179081251L;

	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("se.bengtsson.desc.properties.messages"); //$NON-NLS-1$

	private JPanel contentPane;

	private final ButtonGroup buttonGroup = new ButtonGroup();

	private JTextField textField;
	private JRadioButton rdbtnClassic;
	private JButton btnPlay;
	private JButton btnCancel;
	private JButton btnPicture;

	/**
	 * Create the frame.
	 */
	public NewGameDialog() {

		Handler handler = new Handler();

		setResizable(false);
		setTitle(BUNDLE.getString("NewGameDialog.this.title")); //$NON-NLS-1$
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 297, 174);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		setLocationRelativeTo(null);

		JLabel lblName = new JLabel(BUNDLE.getString("NewGameDialog.lblName.text")); //$NON-NLS-1$
		lblName.setBounds(12, 12, 53, 15);
		contentPane.add(lblName);

		textField = new JTextField();
		textField.addActionListener(handler);
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (textField.getText().length() > 0) {
					btnPlay.setEnabled(true);
				} else {
					btnPlay.setEnabled(false);
				}
			}
		});

		textField.setBounds(70, 10, 101, 19);
		contentPane.add(textField);
		textField.setColumns(10);

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), BUNDLE.getString("NewGameDialog.panel.borderTitle"), TitledBorder.LEADING, TitledBorder.TOP, null, null)); //$NON-NLS-1$
		panel.setBounds(183, 10, 89, 64);
		contentPane.add(panel);
		panel.setLayout(null);

		JRadioButton rdbtnNormal = new JRadioButton(BUNDLE.getString("NewGameDialog.rdbtnNormal.text")); //$NON-NLS-1$
		rdbtnNormal.setBounds(5, 17, 79, 23);
		panel.add(rdbtnNormal);
		rdbtnNormal.setSelected(true);
		buttonGroup.add(rdbtnNormal);

		rdbtnClassic = new JRadioButton(BUNDLE.getString("NewGameDialog.rdbtnClassic.text")); //$NON-NLS-1$
		rdbtnClassic.setBounds(5, 36, 79, 23);
		panel.add(rdbtnClassic);
		buttonGroup.add(rdbtnClassic);

		btnPlay = new JButton(BUNDLE.getString("NewGameDialog.btnPlay.text")); //$NON-NLS-1$
		btnPlay.setEnabled(false);
		btnPlay.setBounds(202, 100, 70, 25);
		contentPane.add(btnPlay);
		btnPlay.addActionListener(handler);

		btnCancel = new JButton(BUNDLE.getString("NewGameDialog.btnCancel.text")); //$NON-NLS-1$
		btnCancel.setBounds(120, 100, 70, 25);
		contentPane.add(btnCancel);
		btnCancel.addActionListener(handler);

		btnPicture = new JButton("");
		btnPicture.setToolTipText(BUNDLE.getString("NewGameDialog.btnPicture.toolTipText")); //$NON-NLS-1$
		btnPicture.setIcon(new ImageIcon(this.getClass().getResource("/icons/player.png")));
		btnPicture.setBounds(70, 39, 63, 57);
		contentPane.add(btnPicture);
		btnPicture.addActionListener(handler);

		JLabel lblPicture = new JLabel(BUNDLE.getString("NewGameDialog.lblPicture.text")); //$NON-NLS-1$
		lblPicture.setBounds(12, 55, 62, 15);
		contentPane.add(lblPicture);

	}

	private class Handler implements ActionListener {

		BufferedImage image = null;

		@Override
		public void actionPerformed(ActionEvent e) {

			// if the user press enter in the textfield
			if (e.getSource() == textField) {
				btnPlay.requestFocusInWindow();
			}

			// if the user clicks the picture button he can load a custom image
			// as portrait
			if (e.getSource() == btnPicture) {

				SwingWorker<BufferedImage, Void> worker = new SwingWorker<BufferedImage, Void>() {

					@Override
					protected BufferedImage doInBackground() throws Exception {

						File path = new File("desc/images");

						if (!path.exists()) {
							path.mkdirs();
						}

						JFileChooser fc = new JFileChooser(path);
						fc.setAcceptAllFileFilterUsed(false);
						fc.setFileFilter(new ImageFilter());
						int choise = fc.showOpenDialog(rootPane);

						if (choise == 0 && fc.getSelectedFile() != null) {

							setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

							Boolean playEnabled = btnPlay.isEnabled();

							textField.setEnabled(false);
							rdbtnClassic.setEnabled(false);
							btnPlay.setEnabled(false);
							btnCancel.setEnabled(false);

							btnPicture.setIcon(new ImageIcon(this.getClass().getResource("/icons/hourglass.gif")));

							try {
								image = ImageIO.read(fc.getSelectedFile());
								image = Scalr.resize(image, 55, 49, null);
								btnPicture.setIcon(new ImageIcon(image));

							} catch (IOException e1) {
								JOptionPane.showMessageDialog(null, BUNDLE.getString("FileIOError.text"), BUNDLE.getString("FileIOError.title.text"), JOptionPane.ERROR_MESSAGE);
							}

							textField.setEnabled(true);
							rdbtnClassic.setEnabled(true);
							if (playEnabled)
								btnPlay.setEnabled(true);
							btnCancel.setEnabled(true);

							setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
						}

						return image;
					}

					@Override
					protected void done() {

						try {
							image = (BufferedImage) get();
						} catch (InterruptedException | ExecutionException e) {

						}
					}

				};

				worker.execute();

			}

			// if user clicks the play button
			if (e.getSource() == btnPlay) {

				File path = new File("desc/images");

				if (!path.exists()) {
					path.mkdirs();
				}

				path = new File("desc/images/" + textField.getText() + ".png");

				int c = 0;
				while (path.exists()) {
					path = new File("desc/images/" + textField.getText() + "_" + c + ".png");
					c++;
				}

				if (image == null) {
					try {
						image = ImageIO.read(this.getClass().getResource("/icons/player.png"));
						path = new File("desc/images/default.png");
					} catch (IOException e2) {
						JOptionPane.showMessageDialog(null, BUNDLE.getString("FileIOError.text"), BUNDLE.getString("FileIOError.title.text"), JOptionPane.ERROR_MESSAGE);
					}
				}

				try {
					ImageIO.write(image, "png", path);

				} catch (IOException e1) {
					JOptionPane.showMessageDialog(null, BUNDLE.getString("FileIOError.text"), BUNDLE.getString("FileIOError.title.text"), JOptionPane.ERROR_MESSAGE);
				}

				GameThread gt = GameThread.getInstance();

				if (gt.isRunning()) {
					gt.stop();
				}

				Game game = new Game(textField.getText(), 1, rdbtnClassic.isSelected(), path.getPath());

				gt.setGameArea(new GameArea(game));

				gt.start();
				setVisible(false);
				dispose();
			}
			if (e.getSource() == btnCancel) {

				setVisible(false);
				dispose();
			}
		}
	}

	public JButton getBtnPicture() {
		return btnPicture;
	}
}
