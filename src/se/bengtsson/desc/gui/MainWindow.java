package se.bengtsson.desc.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;

import se.bengtsson.desc.gui.views.GameView;
import se.bengtsson.desc.gui.views.MainMenuView;
import se.bengtsson.desc.gui.views.Views;
import se.bengtsson.desc.gui.views.components.GameArea;

/**
 * MainWindow singleton
 * 
 * @author Marcus Bengtsson
 * 
 */
public class MainWindow extends JFrame {

	private static final long serialVersionUID = -8788370276343953468L;

	private String title = "Dungeon Esacape";
	private GameView gameView;
	private MainMenuView mainMenuView;

	private static MainWindow instance;

	/**
	 * Create the frame.
	 */
	private MainWindow() {

		setTitle(title);
		setPreferredSize(new Dimension(800, 600));
		setMinimumSize(new Dimension(800, 600));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		getContentPane().setLayout(new BorderLayout(0, 0));
		setLocationRelativeTo(null);
	}

	/**
	 * returns the instance of the Main Window
	 * 
	 * @return the instance of the Main Window
	 */
	public synchronized static MainWindow getInstance() {
		if (instance == null) {
			instance = new MainWindow();
		}

		return instance;
	}

	/**
	 * Choose a view to show on the main-window
	 * 
	 * @param view
	 *            to show
	 */
	private void setView(Views view) {
		getContentPane().removeAll();
		getContentPane().add(view.getPanel(), BorderLayout.CENTER);
	}

	/**
	 * Shows a new game view
	 * 
	 * @param gameArea
	 *            a game are for the game window
	 */
	public void showNewGameView(GameArea gameArea) {
		gameView = new GameView(gameArea);
		setView(gameView);
	}

	/**
	 * Shows a new main menu view
	 */
	public void showNewMainMenuView() {
		mainMenuView = new MainMenuView();
		setView(mainMenuView);
	}

	/**
	 * Returns the current game view
	 * 
	 * @return the current game view
	 */
	public GameView getGameView() {
		return gameView;
	}

	/**
	 * Returns the current main menu view
	 * 
	 * @return the current main menu view
	 */
	public MainMenuView getMainMenuView() {
		return mainMenuView;
	}

}
