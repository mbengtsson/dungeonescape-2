package se.bengtsson.desc.gui;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import se.bengtsson.desc.gui.views.components.DaCube;

/**
 * About dialog
 * 
 * @author Marcus Bengtsson
 * 
 */
public class AboutDialog extends JDialog {

	private static final long serialVersionUID = -8982363864152166855L;

	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("se.bengtsson.desc.properties.messages"); //$NON-NLS-1$

	private DaCube panel;

	/**
	 * Create the dialog.
	 */
	public AboutDialog() {

		setResizable(false);
		setTitle(BUNDLE.getString("AboutDialog.this.title")); //$NON-NLS-1$
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent e) {
				panel.stop();
				dispose();
			}
		});
		setBounds(100, 100, 350, 230);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };

		setLocationRelativeTo(null);

		panel = new DaCube();
		panel.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		getContentPane().add(panel);

		panel.setLayout(gridBagLayout);
		{
			JLabel lblLogo = new JLabel("");
			lblLogo.setIcon(new ImageIcon(this.getClass().getResource("/icons/slogo.png")));
			GridBagConstraints gbc_lblLogo = new GridBagConstraints();
			gbc_lblLogo.anchor = GridBagConstraints.NORTH;
			gbc_lblLogo.insets = new Insets(0, 0, 5, 0);
			gbc_lblLogo.gridx = 0;
			gbc_lblLogo.gridy = 0;
			panel.add(lblLogo, gbc_lblLogo);
		}

		JLabel lblDungeonEscape = new JLabel("Dungeon Escape");
		GridBagConstraints gbc_lblDungeonEscape = new GridBagConstraints();
		gbc_lblDungeonEscape.insets = new Insets(0, 0, 5, 0);
		gbc_lblDungeonEscape.gridx = 0;
		gbc_lblDungeonEscape.gridy = 1;
		panel.add(lblDungeonEscape, gbc_lblDungeonEscape);

		JLabel lblVersion = new JLabel(BUNDLE.getString("AboutDialog.lblVersion.text")); //$NON-NLS-1$
		GridBagConstraints gbc_lblVersion = new GridBagConstraints();
		gbc_lblVersion.insets = new Insets(0, 0, 5, 0);
		gbc_lblVersion.gridx = 0;
		gbc_lblVersion.gridy = 2;
		panel.add(lblVersion, gbc_lblVersion);

		JLabel lblByMarcusBengtsson = new JLabel(BUNDLE.getString("AboutDialog.lblByMarcusBengtsson.text") + "  Marcus Bengtsson, GBJU13, 2013"); //$NON-NLS-1$
		GridBagConstraints gbc_lblByMarcusBengtsson = new GridBagConstraints();
		gbc_lblByMarcusBengtsson.insets = new Insets(0, 0, 5, 0);
		gbc_lblByMarcusBengtsson.gridx = 0;
		gbc_lblByMarcusBengtsson.gridy = 3;
		panel.add(lblByMarcusBengtsson, gbc_lblByMarcusBengtsson);

		JPanel panel_1 = new JPanel();
		panel_1.setOpaque(false);
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.anchor = GridBagConstraints.SOUTHEAST;
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 4;
		panel.add(panel_1, gbc_panel_1);

		JButton btnOk = new JButton(BUNDLE.getString("AboutDialog.btnOk.text")); //$NON-NLS-1$
		btnOk.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		btnOk.setPreferredSize(new Dimension(70, 25));
		btnOk.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				panel.stop();
				dispose();
			}
		});
		panel_1.add(btnOk);

	}

}
