package se.bengtsson.desc.gui;

import java.awt.Font;
import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import se.bengtsson.desc.creatures.Creature;

/**
 * Fight window
 * 
 * @author Marcus Bengtsson
 * 
 */
public class FightWindow extends JDialog {

	private static final long serialVersionUID = -1325421986544599074L;

	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("se.bengtsson.desc.properties.messages"); //$NON-NLS-1$

	private JPanel contentPane;
	private JProgressBar attackerHpBar;
	private JProgressBar attackerPwrBar;
	private JProgressBar defenderHpBar;
	private JProgressBar defenderPwrBar;
	private JLabel defenderPower;
	private JLabel attackerPower;
	private JLabel attackerHealth;
	private JLabel defenderHealth;
	private JLabel lblLevel_1;
	private JLabel lblArmour;
	private JLabel lblWeapon;
	private JLabel lblAttackerIcon;
	private JLabel lblDefenderIcon;

	/**
	 * Create the frame.
	 */
	public FightWindow(Creature attacker, Creature defender) {

		setResizable(false);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setTitle(BUNDLE.getString("FightWindow.title.text") + attacker.getName() + " vs. " + defender.getName());
		setBounds(100, 100, 500, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		setLocationRelativeTo(null);

		JPanel attackerPanel = new JPanel();
		attackerPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		attackerPanel.setBounds(12, 12, 200, 300);
		contentPane.add(attackerPanel);
		attackerPanel.setLayout(null);

		JLabel lblAttacker = new JLabel(attacker.getName());
		lblAttacker.setFont(new Font("Dialog", Font.BOLD, 14));
		lblAttacker.setBounds(62, 12, 138, 15);
		attackerPanel.add(lblAttacker);

		JLabel lblLevel = new JLabel(BUNDLE.getString("FightWindow.lblLevel.text") + attacker.getLevel());
		lblLevel.setBounds(62, 28, 70, 15);
		attackerPanel.add(lblLevel);

		attackerHealth = new JLabel(BUNDLE.getString("FightWindow.lblHealth.text") + attacker.getCurrentHp() + "/" + attacker.getTotalHp());
		attackerHealth.setBounds(8, 55, 176, 15);
		attackerPanel.add(attackerHealth);

		attackerHpBar = new JProgressBar();
		attackerHpBar.setMaximum(attacker.getTotalHp());
		attackerHpBar.setValue(attacker.getCurrentHp());
		attackerHpBar.setBounds(8, 70, 148, 14);
		attackerPanel.add(attackerHpBar);

		attackerPower = new JLabel(BUNDLE.getString("FightWindow.lblPower.text") + attacker.getCurrentPwr() + "/" + attacker.getTotalPwr());
		attackerPower.setBounds(8, 85, 176, 15);
		attackerPanel.add(attackerPower);

		attackerPwrBar = new JProgressBar();
		attackerPwrBar.setMaximum(attacker.getTotalPwr());
		attackerPwrBar.setValue(attacker.getCurrentPwr());
		attackerPwrBar.setBounds(8, 100, 148, 14);
		attackerPanel.add(attackerPwrBar);

		JLabel attackerArmourLabel = new JLabel(attacker.getArmour().getName() + " (+" + attacker.getArmour().getItemStat() + " " + BUNDLE.getString("FightWindow.lblHp.text") + ")");
		attackerArmourLabel.setBounds(8, 140, 176, 15);
		attackerPanel.add(attackerArmourLabel);

		JLabel attackerWeaponLabel = new JLabel(attacker.getWeapon().getName() + " (+" + attacker.getWeapon().getItemStat() + " " + BUNDLE.getString("FightWindow.lblPwr.text") + ")");
		attackerWeaponLabel.setBounds(8, 170, 176, 15);
		attackerPanel.add(attackerWeaponLabel);

		lblArmour = new JLabel(BUNDLE.getString("FightWindow.lblArmour.text")); //$NON-NLS-1$
		lblArmour.setBounds(8, 125, 70, 15);
		attackerPanel.add(lblArmour);

		lblWeapon = new JLabel(BUNDLE.getString("FightWindow.lblWeapon.text")); //$NON-NLS-1$
		lblWeapon.setBounds(8, 155, 70, 15);
		attackerPanel.add(lblWeapon);

		lblAttackerIcon = new JLabel("");
		lblAttackerIcon.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		lblAttackerIcon.setIcon(new ImageIcon(attacker.getPicturePathURL()));
		lblAttackerIcon.setBounds(4, 4, 55, 49);
		attackerPanel.add(lblAttackerIcon);

		JPanel defenderPanel = new JPanel();
		defenderPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		defenderPanel.setBounds(280, 12, 200, 300);
		contentPane.add(defenderPanel);
		defenderPanel.setLayout(null);

		JLabel lblDefender = new JLabel(defender.getName());
		lblDefender.setFont(new Font("Dialog", Font.BOLD, 14));
		lblDefender.setBounds(62, 12, 138, 15);
		defenderPanel.add(lblDefender);

		lblLevel_1 = new JLabel(BUNDLE.getString("FightWindow.lblLevel.text") + defender.getLevel());
		lblLevel_1.setBounds(62, 28, 70, 15);
		defenderPanel.add(lblLevel_1);

		defenderHealth = new JLabel(BUNDLE.getString("FightWindow.lblHealth.text") + defender.getCurrentHp() + "/" + defender.getTotalHp());
		defenderHealth.setBounds(8, 55, 176, 15);
		defenderPanel.add(defenderHealth);

		defenderHpBar = new JProgressBar();
		defenderHpBar.setMaximum(defender.getTotalHp());
		defenderHpBar.setValue(defender.getCurrentHp());
		defenderHpBar.setBounds(8, 70, 148, 14);
		defenderPanel.add(defenderHpBar);

		defenderPower = new JLabel(BUNDLE.getString("FightWindow.lblPower.text") + defender.getCurrentPwr() + "/" + defender.getTotalPwr());
		defenderPower.setBounds(8, 85, 176, 15);
		defenderPanel.add(defenderPower);

		defenderPwrBar = new JProgressBar();
		defenderPwrBar.setMaximum(defender.getTotalPwr());
		defenderPwrBar.setValue(defender.getCurrentPwr());
		defenderPwrBar.setBounds(8, 100, 148, 14);
		defenderPanel.add(defenderPwrBar);

		JLabel defenderArmourLabel = new JLabel(defender.getArmour().getName() + " (+" + defender.getArmour().getItemStat() + " " + BUNDLE.getString("FightWindow.lblHp.text") + ")");
		defenderArmourLabel.setBounds(8, 140, 176, 15);
		defenderPanel.add(defenderArmourLabel);

		JLabel defenderWeaponLabel = new JLabel(defender.getWeapon().getName() + " (+" + defender.getWeapon().getItemStat() + " " + BUNDLE.getString("FightWindow.lblPwr.text") + ")");
		defenderWeaponLabel.setBounds(8, 170, 176, 15);
		defenderPanel.add(defenderWeaponLabel);

		lblArmour = new JLabel(BUNDLE.getString("FightWindow.lblArmour.text"));
		lblArmour.setBounds(8, 125, 70, 15);
		defenderPanel.add(lblArmour);

		lblWeapon = new JLabel(BUNDLE.getString("FightWindow.lblWeapon.text"));
		lblWeapon.setBounds(8, 155, 70, 15);
		defenderPanel.add(lblWeapon);

		lblDefenderIcon = new JLabel("");
		lblDefenderIcon.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		lblDefenderIcon.setIcon(new ImageIcon(defender.getPicturePathURL()));
		lblDefenderIcon.setBounds(4, 4, 55, 49);
		defenderPanel.add(lblDefenderIcon);

	}

	public JProgressBar getAttackerHpBar() {
		return attackerHpBar;
	}

	public JProgressBar getAttackerPwrBar() {
		return attackerPwrBar;
	}

	public JProgressBar getDefenderHpBar() {
		return defenderHpBar;
	}

	public JProgressBar getDefenderPwrBar() {
		return defenderPwrBar;
	}

	public JLabel getDefenderPower() {
		return defenderPower;
	}

	public JLabel getAttackerPower() {
		return attackerPower;
	}

	public JLabel getAttackerHealth() {
		return attackerHealth;
	}

	public JLabel getDefenderHealth() {
		return defenderHealth;
	}
}
