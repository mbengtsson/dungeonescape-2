package se.bengtsson.desc.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

/**
 * Help dialog
 * 
 * @author Marcus Bengtsson
 * 
 */
public class HelpDialog extends JDialog {

	private static final long serialVersionUID = -9063994440572543613L;

	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("se.bengtsson.desc.properties.messages"); //$NON-NLS-1$

	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	public HelpDialog() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 588, 345);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		setLocationRelativeTo(null);

		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);

		JTextArea txtrHelp = new JTextArea();
		txtrHelp.setTabSize(2);
		txtrHelp.setEditable(false);
		txtrHelp.setText(BUNDLE.getString("HelpWindow.txtrHelp.text")); //$NON-NLS-1$
		scrollPane.setViewportView(txtrHelp);

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.SOUTH);

		JButton btnOk = new JButton(BUNDLE.getString("HelpWindow.btnOk.text")); //$NON-NLS-1$
		btnOk.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();

			}
		});
		panel.add(btnOk);
	}
}
