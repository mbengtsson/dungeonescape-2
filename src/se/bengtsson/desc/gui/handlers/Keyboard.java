package se.bengtsson.desc.gui.handlers;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.BitSet;

/**
 * The key listener
 * 
 * @author Marcus Bengtsson
 * 
 */
public class Keyboard implements KeyListener {

	private BitSet keyPressedBits = new BitSet();

	@Override
	public void keyTyped(KeyEvent e) {

	}

	@Override
	public void keyPressed(KeyEvent e) {
		keyPressedBits.set(e.getKeyCode());

	}

	@Override
	public void keyReleased(KeyEvent e) {
		keyPressedBits.clear(e.getKeyCode());

	}

	/**
	 * Checks if a key is pressed
	 * 
	 * @param keyCode
	 *            to check
	 * @return true if pressed
	 */
	public boolean isKeyPressed(int keyCode) {
		return keyPressedBits.get(keyCode);

	}

	/**
	 * Clears all pressed keys
	 */
	public void clearKeyPressed() {
		keyPressedBits.clear();
	}
}