package se.bengtsson.desc.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import se.bengtsson.desc.creatures.player.Player;
import se.bengtsson.desc.items.Item;

/**
 * Inventory window
 * 
 * @author Marcus Bengtsson
 * 
 */
public class InventoryWindow extends JDialog {

	private static final long serialVersionUID = -296643934943878786L;

	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("se.bengtsson.desc.properties.messages"); //$NON-NLS-1$

	private JPanel contentPane;

	private Player player;

	private DefaultListModel<Item> allItems;
	private DefaultListModel<Item> armItems;
	private DefaultListModel<Item> wepItems;

	private JList<Item> allList;
	private JList<Item> armList;
	private JList<Item> wepList;

	private JButton btnAllEquip;
	private JButton btnArmEquip;
	private JButton btnWepEquip;
	private JButton btnAllDrop;
	private JButton btnArmDrop;
	private JButton btnWepDrop;
	private JButton btnBack;
	private JLabel lblHp;
	private JLabel lblPwr;
	private JLabel lblArmour;
	private JLabel lblWeapon;

	/**
	 * Create the frame.
	 */
	public InventoryWindow(Player player) {

		this.player = player;

		Handler handler = new Handler();

		setTitle(BUNDLE.getString("InventoryWindow.this.title")); //$NON-NLS-1$
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		setLocationRelativeTo(null);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.LEFT);
		tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
		contentPane.add(tabbedPane, BorderLayout.CENTER);

		JPanel allPanel = new JPanel();
		tabbedPane.addTab(BUNDLE.getString("InventoryWindow.panelAll.text"), null, allPanel, null);
		allPanel.setLayout(new BorderLayout(0, 0));

		JScrollPane allScrollPane = new JScrollPane();
		allPanel.add(allScrollPane, BorderLayout.CENTER);

		allItems = new DefaultListModel<Item>();

		for (Item item : player.getInv().getItems()) {
			allItems.addElement(item);
		}

		allList = new JList<Item>();
		allList.setModel(allItems);

		allList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		allScrollPane.setViewportView(allList);

		JPanel panel_1 = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel_1.getLayout();
		flowLayout.setAlignment(FlowLayout.RIGHT);
		allPanel.add(panel_1, BorderLayout.SOUTH);

		btnAllEquip = new JButton(BUNDLE.getString("InventoryWindow.btnAllEquip.text")); //$NON-NLS-1$
		btnAllEquip.setActionCommand("Equip");
		btnAllEquip.addActionListener(handler);
		panel_1.add(btnAllEquip);

		btnAllDrop = new JButton(BUNDLE.getString("InventoryWindow.btnAllDrop.text")); //$NON-NLS-1$
		btnAllDrop.setActionCommand("Drop");
		btnAllDrop.addActionListener(handler);
		panel_1.add(btnAllDrop);

		JPanel armPanel = new JPanel();
		tabbedPane.addTab(BUNDLE.getString("InventoryWindow.panelArm.text"), null, armPanel, null);
		armPanel.setLayout(new BorderLayout(0, 0));

		JScrollPane armScrollPane = new JScrollPane();
		armPanel.add(armScrollPane, BorderLayout.CENTER);

		armItems = new DefaultListModel<Item>();

		for (Item item : player.getInv().getArmours()) {
			armItems.addElement(item);
		}

		armList = new JList<Item>();
		armList.setModel(armItems);

		armList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		armScrollPane.setViewportView(armList);

		JPanel panel_2 = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) panel_2.getLayout();
		flowLayout_1.setAlignment(FlowLayout.RIGHT);
		armPanel.add(panel_2, BorderLayout.SOUTH);

		btnArmEquip = new JButton(BUNDLE.getString("InventoryWindow.btnAllEquip.text"));
		btnArmEquip.setActionCommand("Equip");
		btnArmEquip.addActionListener(handler);
		panel_2.add(btnArmEquip);

		btnArmDrop = new JButton(BUNDLE.getString("InventoryWindow.btnAllDrop.text"));
		btnArmDrop.setActionCommand("Drop");
		btnArmDrop.addActionListener(handler);
		panel_2.add(btnArmDrop);

		JPanel wepPanel = new JPanel();
		tabbedPane.addTab(BUNDLE.getString("InventoryWindow.panelWep.text"), null, wepPanel, null);
		wepPanel.setLayout(new BorderLayout(0, 0));

		JScrollPane wepScrollPane = new JScrollPane();
		wepPanel.add(wepScrollPane, BorderLayout.CENTER);

		wepItems = new DefaultListModel<Item>();

		for (Item item : player.getInv().getWeapons()) {
			wepItems.addElement(item);
		}

		wepList = new JList<Item>();
		wepList.setModel(wepItems);

		wepList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		wepScrollPane.setViewportView(wepList);

		JPanel panel_3 = new JPanel();
		FlowLayout flowLayout_2 = (FlowLayout) panel_3.getLayout();
		flowLayout_2.setAlignment(FlowLayout.RIGHT);
		wepPanel.add(panel_3, BorderLayout.SOUTH);

		btnWepEquip = new JButton(BUNDLE.getString("InventoryWindow.btnAllEquip.text"));
		btnWepEquip.setActionCommand("Equip");
		btnWepEquip.addActionListener(handler);
		panel_3.add(btnWepEquip);

		btnWepDrop = new JButton(BUNDLE.getString("InventoryWindow.btnAllDrop.text"));
		btnWepDrop.setActionCommand("Drop");
		btnWepDrop.addActionListener(handler);
		panel_3.add(btnWepDrop);

		JPanel bottomPanel = new JPanel();
		FlowLayout fl_bottomPanel = (FlowLayout) bottomPanel.getLayout();
		fl_bottomPanel.setAlignment(FlowLayout.RIGHT);
		contentPane.add(bottomPanel, BorderLayout.SOUTH);

		btnBack = new JButton(BUNDLE.getString("InventoryWindow.btnBack.text")); //$NON-NLS-1$
		btnBack.addActionListener(handler);
		bottomPanel.add(btnBack);

		JPanel sidePanel = new JPanel();
		sidePanel.setPreferredSize(new Dimension(150, 10));
		sidePanel.setSize(new Dimension(150, 0));
		sidePanel.setMinimumSize(new Dimension(150, 10));
		contentPane.add(sidePanel, BorderLayout.EAST);
		sidePanel.setLayout(null);

		JLabel lblName = new JLabel(player.getName());
		lblName.setBounds(12, 12, 126, 15);
		sidePanel.add(lblName);

		lblHp = new JLabel(BUNDLE.getString("InventoryWindow.lblHp.text") + player.getCurrentHp() + " (" + player.getHp() + "+" + player.getArmour().getItemStat() + ")");
		lblHp.setBounds(12, 27, 126, 15);
		sidePanel.add(lblHp);

		lblPwr = new JLabel(BUNDLE.getString("InventoryWindow.lblPwr.text") + player.getCurrentPwr() + " (" + player.getPwr() + "+" + player.getWeapon().getItemStat() + ")");
		lblPwr.setBounds(12, 42, 126, 15);
		sidePanel.add(lblPwr);

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), BUNDLE.getString("InventoryWindow.panel.borderTitle"), TitledBorder.LEADING, TitledBorder.TOP, null, null)); //$NON-NLS-1$
		panel.setBounds(2, 69, 136, 82);
		sidePanel.add(panel);
		panel.setLayout(null);

		lblArmour = new JLabel(player.getArmour().getName() + " +" + player.getArmour().getItemStat() + " " + BUNDLE.getString("InventoryWindow.lblHp.text"));
		lblArmour.setBounds(5, 30, 126, 15);
		panel.add(lblArmour);

		lblWeapon = new JLabel(player.getWeapon().getName() + " +" + player.getWeapon().getItemStat() + " " + BUNDLE.getString("InventoryWindow.lblPwr.text"));
		lblWeapon.setBounds(5, 60, 126, 15);
		panel.add(lblWeapon);

		JLabel lblArmour_1 = new JLabel(BUNDLE.getString("InventoryWindow.lblArmour_1.text")); //$NON-NLS-1$
		lblArmour_1.setBounds(5, 15, 70, 15);
		panel.add(lblArmour_1);

		JLabel lblWeapon_1 = new JLabel(BUNDLE.getString("InventoryWindow.lblWeapon_1.text")); //$NON-NLS-1$
		lblWeapon_1.setBounds(5, 45, 70, 15);
		panel.add(lblWeapon_1);

	}

	private class Handler implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {

			Item item = null;
			int index;

			if (e.getSource() == btnAllEquip || e.getSource() == btnAllDrop) {

				index = allList.getSelectedIndex();

				if (index < player.getInv().getItems().size() && index >= 0) {
					item = player.getInv().getItems().get(index);
				}

			}
			if (e.getSource() == btnArmEquip || e.getSource() == btnArmDrop) {

				index = armList.getSelectedIndex();

				if (index < player.getInv().getArmours().size() && index >= 0) {
					item = player.getInv().getArmours().get(index);
				}
			}
			if (e.getSource() == btnWepEquip || e.getSource() == btnWepDrop) {

				index = wepList.getSelectedIndex();

				if (index < player.getInv().getWeapons().size() && index >= 0) {
					item = player.getInv().getWeapons().get(index);
				}
			}

			if (e.getActionCommand().equals("Equip") && item != null) {
				equipItem(item);
				player.getInv().removeItemById(item.ID);
			}

			if (e.getActionCommand().equals("Drop") && item != null) {
				player.getInv().removeItemById(item.ID);
			}

			if (e.getSource() == btnBack) {
				setVisible(false);
				dispose();
			}

			refresh();

		}

		/**
		 * refreshes the inventory window
		 */
		private void refresh() {

			allItems = new DefaultListModel<Item>();

			for (Item item : player.getInv().getItems()) {
				allItems.addElement(item);
			}

			armItems = new DefaultListModel<Item>();

			for (Item item : player.getInv().getArmours()) {
				armItems.addElement(item);
			}

			wepItems = new DefaultListModel<Item>();

			for (Item item : player.getInv().getWeapons()) {
				wepItems.addElement(item);
			}

			allList.setModel(allItems);
			armList.setModel(armItems);
			wepList.setModel(wepItems);

			lblHp.setText(BUNDLE.getString("InventoryWindow.lblHp.text") + player.getCurrentHp() + " (" + player.getHp() + "+" + player.getArmour().getItemStat() + ")");
			lblPwr.setText(BUNDLE.getString("InventoryWindow.lblPwr.text") + player.getCurrentPwr() + " (" + player.getPwr() + "+" + player.getWeapon().getItemStat() + ")");
			lblArmour.setText(player.getArmour().getName() + " +" + player.getArmour().getItemStat() + " " + BUNDLE.getString("InventoryWindow.lblHp.text"));
			lblWeapon.setText(player.getWeapon().getName() + " +" + player.getWeapon().getItemStat() + " " + BUNDLE.getString("InventoryWindow.lblPwr.text"));

		}

		/**
		 * Equip player with an item
		 * 
		 * @param item
		 *            to equip
		 * @return true if success, false if fail
		 */
		private boolean equipItem(Item item) {

			if (item == null) {
				return false;
			}

			if (item.getType() == 1) {
				if (!player.getWeapon().getName().equals(BUNDLE.getString("Item.empty.empty.text"))) {
					player.getInv().addItem(player.getWeapon());
				}
				player.setWeapon(item);
				return true;
			} else if (item.getType() == 2) {
				if (!player.getArmour().getName().equals(BUNDLE.getString("Item.empty.empty.text"))) {
					player.getInv().addItem(player.getArmour());
				}
				player.setArmour(item);
				return true;
			}

			return false;
		}

	}
}
