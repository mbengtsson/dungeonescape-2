package se.bengtsson.desc.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import se.bengtsson.desc.game.GameThread;

/**
 * Game over dialog
 * 
 * @author Marcus Bengtsson
 * 
 */
public class GameOverDialog extends JDialog {

	private static final long serialVersionUID = -3773668366754131464L;

	private final JPanel contentPanel = new JPanel();

	JButton btnOk;

	GameThread gt = GameThread.getInstance();

	/**
	 * Create the dialog.
	 */
	public GameOverDialog() {

		Handler handler = new Handler();

		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		setBounds(400, 300, 319, 142);
		setTitle("Game Over");
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);

		setLocationRelativeTo(null);

		contentPanel.setLayout(null);
		{
			JLabel icon = new JLabel("");
			icon.setIcon(new ImageIcon(this.getClass().getResource("/icons/gameover.png")));
			icon.setBounds(0, 0, 68, 93);
			contentPanel.add(icon);
		}
		{
			JLabel lblGameOverMan = new JLabel("Game over man, GAME OVER!!");
			lblGameOverMan.setBounds(80, 25, 208, 15);
			contentPanel.add(lblGameOverMan);
		}

		btnOk = new JButton("Ok");
		btnOk.addActionListener(handler);
		btnOk.setBounds(220, 68, 68, 25);
		contentPanel.add(btnOk);

		gt.halt();

	}

	/**
	 * Action listener
	 * 
	 * @author Marcus Bengtsson
	 * 
	 */
	private class Handler implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == btnOk) {

				if (gt.isRunning()) {
					gt.stop();
				}

				MainWindow.getInstance().showNewMainMenuView();
				MainWindow.getInstance().setVisible(true);

				setVisible(false);
				dispose();
			}
		}
	}
}
