package se.bengtsson.desc.creatures.player;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import se.bengtsson.desc.creatures.Creature;
import se.bengtsson.desc.creatures.Stats;

/**
 * Player class, creates a player
 * 
 * @author Marcus Bengtsson
 * 
 */
public class Player extends Creature {

	/**
	 * Constructor, creates a new player
	 * 
	 * @param level
	 *            players initial level
	 * @param name
	 *            players name
	 * @param sign
	 *            sign to represent player on the map
	 */
	public Player(int level, String name, char sign) {

		super(level, name, sign, Stats.PLAYER.getInvSize());

		hp = rndStat(Stats.PLAYER.getHpMin(), Stats.PLAYER.getHpMax());
		totalHp = hp;
		currentHp = totalHp;

		pwr = rndStat(Stats.PLAYER.getPwrMin(), Stats.PLAYER.getPwrMax());
		totalPwr = pwr;
		currentPwr = totalPwr;
		
		setPicturePath("/icons/player.png");
	}
	
	/**
	 * Constructor, creates a new player with custom portrait
	 * @param level players initial level
	 * @param name players name
	 * @param sign sign to rpresent player on the map
	 * @param picPath path to portrait image-file
	 */
	public Player(int level, String name, char sign, String picPath) {

		super(level, name, sign, Stats.PLAYER.getInvSize());

		hp = rndStat(Stats.PLAYER.getHpMin(), Stats.PLAYER.getHpMax());
		totalHp = hp;
		currentHp = totalHp;

		pwr = rndStat(Stats.PLAYER.getPwrMin(), Stats.PLAYER.getPwrMax());
		totalPwr = pwr;
		currentPwr = totalPwr;
		
		setPicturePath(picPath);
	}

	/**
	 * Level up player if experience is greater or equal to level² * 5
	 * 
	 * @return true if success, false if fail
	 */
	@Override
	public boolean levelUp() {

		if (getXp() >= getLevel() * getLevel() * 5) {

			level++;
			hp += 3;
			pwr += 3;

			totalHp = hp + getArmour().getItemStat();
			totalPwr = pwr + getWeapon().getItemStat();
			currentHp = totalHp;
			currentPwr = totalPwr;

			return true;
		}

		return false;
	}
	
	@Override
	public URL getPicturePathURL() {
		
		try {
			return new File(picturePath).toURI().toURL();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
}
