package se.bengtsson.desc.creatures.player;

import java.util.ArrayList;

import se.bengtsson.desc.items.Item;

/**
 * Inventory class, creates and handles inventories
 * size and weight are currently unimplemented but left 
 * for future use
 * 
 * @author Marcus Bengtsson
 * 
 */
public class Inventory {

	private int size;
	private int weight;

	private ArrayList<Item> items;

	/**
	 * creates a new inventory
	 * 
	 * @param size
	 *            inventory size
	 */
	public Inventory(int size) {
		this.size = size;
		items = new ArrayList<Item>();
	}

	/**
	 * Adds an item to the inventory
	 * 
	 * @param item
	 *            item to add
	 * @return true if success, false if fail
	 */
	public boolean addItem(Item item) {

		if (items.add(item)) {
			weight += item.getWeight();
			return true;
		}
		return false;
	}

	/**
	 * Removes one item from the inventory by name
	 * 
	 * @param name
	 *            name of the item to remove
	 * @return the removed item
	 */
	public Item removeItem(String name) {

		for (Item item : items) {
			if (item.getName().toLowerCase().equals(name.toLowerCase())) {
				weight -= item.getWeight();
				items.remove(item);
				return item;

			}
		}
		return null;
	}
	
	/**
	 * Removes one item from the inventory by item id
	 * 
	 * @param id the id of the item to remove
	 * @return the removed item
	 */
	public Item removeItemById(int id) {

		for (Item item : items) {
			if (item.ID == id) {
				weight -= item.getWeight();
				items.remove(item);
				return item;

			}
		}
		return null;
	}

	/**
	 * removes an item from the inventory by index
	 * 
	 * @param i
	 *            index
	 * @return the removed item
	 */
	public Item removeItem(int i) {

		weight -= items.get(i).getWeight();
		return items.remove(i);

	}

	/**
	 * Gets the weight of all items in the inventory
	 * 
	 * @return weight
	 */
	public int getWeight() {
		return weight;
	}

	/**
	 * get an item from the inventory
	 * 
	 * @param i
	 *            index
	 * @return the item
	 */
	public Item getItem(int i) {
		return items.get(i);
	}

	/**
	 * Get the inventory as an array list
	 * 
	 * @return the inventory
	 */
	public ArrayList<Item> getItems() {
		return items;
	}
	
	/**
	 * Returns inventory max size
	 * 
	 * @return inventory max size
	 */
	public int getSize(){
		return size;
	}

	/**
	 * get all weapons in the inventory as an array list
	 * 
	 * @return the weapons
	 */
	public ArrayList<Item> getWeapons() {
		ArrayList<Item> weapons = new ArrayList<Item>();

		for (Item item : items) {
			if (item.getType() == 1) {
				weapons.add(item);
			}
		}

		return weapons;
	}

	/**
	 * get all armour in the inventory as an array list
	 * 
	 * @return the armours
	 */
	public ArrayList<Item> getArmours() {
		ArrayList<Item> armours = new ArrayList<Item>();

		for (Item item : items) {
			if (item.getType() == 2) {
				armours.add(item);
			}
		}
		return armours;
	}
}
