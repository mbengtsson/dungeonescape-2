package se.bengtsson.desc.creatures.npc;

import se.bengtsson.desc.creatures.Stats;

/**
 * Boss class, creates bosses
 * 
 * @author Marcus Bengtsson
 * 
 */
public class Boss extends Npc {

	/**
	 * Constructor, creates a new boss npc
	 * 
	 * @param level
	 *            initial level
	 * @param name
	 *            npc name
	 */
	public Boss(int level, String name) {
		super(level, name, 'B', Stats.BOSS_MOB.getInvSize(), true);

		hp = rndStat(Stats.BOSS_MOB.getHpMin(), Stats.BOSS_MOB.getHpMax()) + (level - 1) * 3;
		totalHp = hp;
		currentHp = totalHp;

		pwr = rndStat(Stats.BOSS_MOB.getPwrMin(), Stats.BOSS_MOB.getPwrMax()) + (level - 1) * 3;
		totalPwr = pwr;
		currentPwr = totalPwr;
		
		setPicturePath("/icons/boss.png");
	}

}
