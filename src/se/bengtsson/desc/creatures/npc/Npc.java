package se.bengtsson.desc.creatures.npc;

import java.util.Random;

import se.bengtsson.desc.creatures.Creature;
import se.bengtsson.desc.creatures.player.Player;
import se.bengtsson.desc.dungeon.Maps;

/**
 * Abstract npc class, all npc classes inherits this class
 * 
 * 
 * @author Marcus Bengtsson
 * 
 */
public abstract class Npc extends Creature implements Ai {

	private boolean agressive;

	private int lastMove = -1;

	public Npc(int level, String name, char sign, int invSize, boolean agressive) {
		super(level, name, sign, invSize);

		this.agressive = agressive;

	}

	// npc move logic
	@Override
	public boolean moveNpc(Maps map, Player player) {

		Random rnd = new Random();

		int direction = detectPlayer(player);

		if (direction < 0 || agressive == false) {

			if (lastMove == -1) {
				// first move
				direction = rnd.nextInt(4);
			} else {
				// higher number makes it more likely npc will move in the same
				// direction if possible
				direction = rnd.nextInt(20);
			}

			if (direction > 3) {
				direction = lastMove;
			}

			lastMove = direction;

		}

		int collision = -1;

		switch (direction) {
		case 0:
			collision = move(map, "north");
			break;
		case 1:
			collision = move(map, "south");
			break;
		case 2:
			collision = move(map, "west");
			break;
		case 3:
			collision = move(map, "east");
			break;
		}

		if (collision == 3) {
			return true;
		}

		return false;
	}

	/**
	 * Detects player if the player within 7 tiles in all directions and returns
	 * direction to the player
	 * 
	 * @param player
	 *            player to detect
	 * @return 0 if player is up, 1 if player is down, 2 if player is left, 3 if
	 *         player is right, -1 if player isn't in range
	 */

	public int detectPlayer(Player player) {

		// detect up
		if ((getY() > player.getY() && (getY() - player.getY()) <= 7) && Math.abs(getY() - player.getY()) >= Math.abs(getX() - player.getX())) {
			return 0;
		}

		// detect down
		if ((getY() < player.getY() && (player.getY() - getY()) <= 7) && Math.abs(getY() - player.getY()) >= Math.abs(getX() - player.getX())) {
			return 1;
		}

		// detect left
		if ((getX() > player.getX() && (getX() - player.getX()) <= 7) && Math.abs(getX() - player.getX()) >= Math.abs(getY() - player.getY())) {
			return 2;
		}

		// detect right
		if ((getX() < player.getX() && (player.getX() - getX()) <= 7) && Math.abs(getX() - player.getX()) >= Math.abs(getY() - player.getY())) {
			return 3;
		}

		return -1;
	}

	@Override
	public int move(Maps map, String direction) {

		int newX;
		int newY;

		switch (direction) {
		case "north":
			newX = getX();
			newY = getY() - 1;
			break;
		case "south":
			newX = getX();
			newY = getY() + 1;
			break;
		case "west":
			newX = getX() - 1;
			newY = getY();
			break;
		case "east":
			newX = getX() + 1;
			newY = getY();
			break;
		default:
			newX = getX();
			newY = getY();
			break;
		}

		int type = collisionCheck(map, newX, newY);

		if (type == 0 || type == 3) {
			setLocation(newX, newY);

		}
		return type;

	}
}
